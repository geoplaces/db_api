"""App provider."""
from os import getenv as ge
from pathlib import Path

import sentry_sdk
from dotenv import load_dotenv
from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles

from .v1 import create_router_v1
from .website.roots import website_router

load_dotenv()
dsn = ge('SENTRY_DSN')


def sampler(sampling_context) -> float:
    try:
        name = sampling_context['transaction_context']['name']
        if '/auth/status' in name:
            return 0.
    except KeyError:
        return 1.

    return 1.


if dsn is not None and dsn != '':
    sentry_sdk.init(
        dsn=dsn,
        enable_tracing=True,
        traces_sampler=sampler,
        profiles_sampler=sampler
    )

app = FastAPI(title='Places database API',
              description='Places API for crawlers and web apps',
              version='1'
              )


def app_init():
    app.include_router(create_router_v1())
    app.include_router(website_router)
    app.mount("/static", StaticFiles(directory=f'{Path(__file__).parent}/website/static'), name="static")
    return app
