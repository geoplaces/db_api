from datetime import datetime

from fastapi import HTTPException, status
from sqlalchemy import String, literal_column, select, Select
from sqlalchemy.orm import Session

from app.models import SourceTable, AssociateTable, TargetTable
from app.v1.models_utils import target_tables, associate_tables, link_column, source_tables
from app.v1.schemas import SourceEnum, TargetEnum

DUMMY_ID = 'dummy'
WRONG_SOURCE_ERROR = "Wrong associate source"


def get_associate_table(associate_source: SourceEnum) -> AssociateTable:
    table = associate_tables.get(associate_source)
    if table is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=WRONG_SOURCE_ERROR
        )
    return table


def get_source_table(associate_source: SourceEnum) -> AssociateTable:
    table = source_tables.get(associate_source)
    if table is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=WRONG_SOURCE_ERROR
        )
    return table


def get_within_coords_utils(source_table: SourceTable,
                            target_table: TargetTable,
                            associate_table: AssociateTable) -> Select:
    """Prepare join query for a coordinates' lookup."""
    return select(literal_column(f"{target_table.__tablename__}.location[0]", String).label('lat'),
                  literal_column(f"{target_table.__tablename__}.location[1]", String).label('lon'),
                  source_table.name.label('name'),
                  source_table.id.label('id'),
                  source_table.categoryName.label('cat'),
                  source_table.address.label('address'),
                  source_table.city.label('city')
                  ).select_from(target_table).join(associate_table).join(source_table)


def add_dummy(db: Session, table: TargetTable) -> TargetTable:
    """Create a dummy record."""
    dummy_place = table(id=DUMMY_ID, updated_at=datetime.now())
    db.add(dummy_place)
    db.commit()
    return dummy_place


def add_dummy_link(db: Session,
                   target: TargetEnum,
                   associate_source: SourceEnum,
                   associate_id: int) -> None:
    """Link a dummy record."""
    table = target_tables[target]
    dummy = db.query(table.id).where(table.id == DUMMY_ID).first()
    if not dummy:
        dummy = add_dummy(db, table)

    associate_table = associate_tables[associate_source]
    associate = db.query(associate_table).filter(
        associate_table.id == associate_id).first()
    if not associate:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Associate id not found"
        )

    setattr(associate, link_column[associate_source][target].name, dummy.id)
    db.commit()

x_grad = 1.3615356822108318e-05
x_intr = -5.832692470523333
y_grad = 8.965168156107985e-06
y_intr = 27.082942839497104

def coordinates_to_xy(lon: float, lat: float) -> tuple[int, int]:
    return int((lon - x_intr) / x_grad), int((lat - y_intr) / y_grad)

def xy_to_coordinates(x: int, y: int) -> tuple[float, float]:
    return x_grad * x + x_intr, y_grad * y + y_intr
