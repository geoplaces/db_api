from datetime import timedelta
from typing import List, Dict

from fastapi import APIRouter, status, Depends, HTTPException
from sqlalchemy import update, select, delete, func
from sqlalchemy.dialects.postgresql import insert
from sqlalchemy.orm import Session

from app.db import get_db
from app.models import AssociateTable, AnyPlaceTable, EdenredAssociate, DejupAssociate
from app.v1.auth import get_scoped_reader, get_scoped_writer, oauth2_scheme
from app.v1.models_utils import source_tables, target_tables, \
    associate_tables, link_source_id, edenred_link_column, dejup_link_column, to_dict
from app.v1.places_utils import get_place_table, create_associate_records, places_record_creators
from app.v1.schemas import SourceEnum, PlaceEnum

place = APIRouter(
    prefix="/places",
    tags=["Places"],
    responses={404: {"description": "Not found"}},
)


@place.get('/{place_source}/{place_id}', status_code=status.HTTP_200_OK)
async def get_place(place_source: PlaceEnum,
                    place_id: str | int,
                    table: AnyPlaceTable = Depends(get_place_table),
                    token: str = Depends(oauth2_scheme),
                    db: Session = Depends(get_db),
                    ):
    """Get particular place by ID."""
    await get_scoped_reader(place_source, token, db)
    db_place = db.execute(select(table).where(table.id == place_id)).scalars().one_or_none()
    if not db_place:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Place `{place_id}` from `{table.__tablename__}` not found"
        )
    return db_place


@place.post('/{place_source}/batch', status_code=status.HTTP_201_CREATED)
async def post_places_endpoint(place_source: PlaceEnum,
                               places: List[Dict],
                               table: AnyPlaceTable = Depends(get_place_table),
                               token: str = Depends(oauth2_scheme),
                               db: Session = Depends(get_db),
                               ):
    """Post a bunch of places."""
    await get_scoped_writer(place_source, token, db)
    # schema validation
    creator = places_record_creators.get(place_source)
    db_places = [creator(post_place) for post_place in places]
    serialised_places = [to_dict(db_place) for db_place in db_places]
    if len(serialised_places) != 0:
        db.execute(insert(table).values(serialised_places).on_conflict_do_nothing())

    # only for source tables --> insert record to associate table
    associate_table: AssociateTable = associate_tables.get(place_source)
    if associate_table is not None:
        create_associate_records([db_place.id for db_place in db_places], place_source, associate_table, db)

    db.commit()


@place.put('/{place_source}/batch', status_code=status.HTTP_200_OK)
async def put_places_endpoint(place_source: PlaceEnum,
                              places: List[Dict],
                              table: AnyPlaceTable = Depends(get_place_table),
                              token: str = Depends(oauth2_scheme),
                              db: Session = Depends(get_db)):
    """Update a bunch of places."""
    await get_scoped_writer(place_source, token, db)
    creator = places_record_creators.get(place_source)

    if table is None or creator is None or creator is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Place source `{place_source}` is unknown"
        )

    # Auth follows 404 check, so that 404 has priority over 401
    await get_scoped_writer(place_source, token, db)
    db_places = [creator(put_place) for put_place in places]
    serialised_places = [to_dict(db_place) for db_place in db_places]
    insert_stm = insert(table).values(serialised_places)
    if len(serialised_places) != 0:
        db.execute(insert_stm.on_conflict_do_update(index_elements=[table.id.name], set_=insert_stm.excluded))

    # only for source tables --> insert record to associate table
    associate_table: AssociateTable = associate_tables.get(place_source)
    if associate_table is not None:
        create_associate_records([db_place.id for db_place in db_places], place_source, associate_table, db)

    db.commit()


@place.delete('/{place_source}/{place_id}', status_code=status.HTTP_204_NO_CONTENT)
async def delete_place(place_source: PlaceEnum,
                       place_id: str,
                       table: AnyPlaceTable = Depends(get_place_table),
                       token: str = Depends(oauth2_scheme),
                       db: Session = Depends(get_db)):
    """Delete place records."""
    await get_scoped_writer(place_source, token, db)
    if place_source in target_tables:
        kwargs = {edenred_link_column[place_source].name: None}
        db.execute(update(EdenredAssociate).values(**kwargs).where(edenred_link_column[place_source].__eq__(place_id)))
        kwargs = {dejup_link_column[place_source].name: None}
        db.execute(update(DejupAssociate).values(**kwargs).where(dejup_link_column[place_source].__eq__(place_id)))
        db.commit()

    if place_source in source_tables:
        db.execute(delete(associate_tables[place_source]).where(link_source_id[place_source].__eq__(place_id)))
        db.commit()

    rows = db.execute(delete(table).where(table.id == place_id)).rowcount
    if rows == 0:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Place {place_id} not found"
        )
    db.commit()


@place.delete('/{place_source}/batch/old', status_code=status.HTTP_204_NO_CONTENT)
async def delete_old_places(place_source: SourceEnum,
                            days: int,
                            table: AnyPlaceTable = Depends(get_place_table),
                            token: str = Depends(oauth2_scheme),
                            db: Session = Depends(get_db)):
    await get_scoped_writer(place_source, token, db)
    """Delete places that are not updated for a while."""
    db.query(associate_tables[place_source]).where(
        link_source_id[place_source] == source_tables[place_source].id
    ).where(
        table.updated_at < func.now() - timedelta(days=days)
    ).delete(synchronize_session=False)

    db.commit()
    db.query(table).where(table.updated_at < func.now() - timedelta(days=days)).delete(synchronize_session=False)
    db.commit()
