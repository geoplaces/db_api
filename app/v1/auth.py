"""Authentication endpoints and helpers."""

from datetime import datetime, timezone, timedelta
from os import getenv as ge
from typing import Type

from dotenv import load_dotenv
from fastapi import APIRouter, status, HTTPException, Depends, Security
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm, SecurityScopes
from jwt import decode, encode, InvalidTokenError, ExpiredSignatureError
from sqlalchemy import text
from sqlalchemy.orm import Session
from werkzeug.security import check_password_hash

from app.db import get_db
from app.models import User

load_dotenv()

auth = APIRouter(
    prefix="/auth",
    tags=["Auth"],
    responses={status.HTTP_404_NOT_FOUND: {"description": "Not found"}},
)


@auth.post("/token")
async def login(form_data: OAuth2PasswordRequestForm = Depends(), db: Session = Depends(get_db)):
    """Generate Bearer token with scopes."""
    user = db.query(User).filter_by(name=form_data.username).first()
    if not user:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail="Incorrect username or password")

    if not check_password_hash(user.pasw, form_data.password + user.salt):
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail="Incorrect username or password")

    exp_time = datetime.now(tz=timezone.utc) + timedelta(hours=1)
    to_encode = {'sub': user.name, 'scopes': form_data.scopes,
                 'exp': exp_time}
    encoded_jwt = encode(to_encode, ge('SECRET'), algorithm=ge('ALGO'))
    return {"access_token": encoded_jwt, "token_type": "bearer", "exp": exp_time.timestamp()}


oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/auth/token",
                                     scopes={
                                         "me": 'Access user',
                                         'read:dejup_locations': 'Read indecies',
                                         'write:dejup_locations': 'Write indecies',
                                         'read:edenred_locations': "Read Edenred scan positions",
                                         'write:edenred_locations': "Read Edenred scan positions",
                                         'read:edenred': "Read Edenred places",
                                         'write:edenred': "Read Edenred places",
                                         'read:dejup': 'Read places stored in DejPlaces',
                                         'write:dejup': 'Write places stored in DejPlaces',
                                         'read:osm': 'Read osm table',
                                         'write:osm': 'Write osm table',
                                         'read:google': 'Read Google table',
                                         'write:google': 'Write Google table',
                                         'read:associate': 'Read associate table',
                                         'write:associate': 'Write associate table'
                                     }
                                     )


async def get_current_user(security_scopes: SecurityScopes,
                           token: str = Depends(oauth2_scheme),
                           db: Session = Depends(get_db)
                           ) -> Type[User]:
    """Base helper for user right management."""
    try:
        payload = decode(token, ge('SECRET'), ge('ALGO'))
    except ExpiredSignatureError:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail="Token expired")
    except InvalidTokenError:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid token")

    user = db.query(User).filter_by(name=payload.get('sub')).first()
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid authentication credentials"
        )
    for scope in security_scopes.scopes:
        if scope not in payload.get('scopes'):
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED, detail="Not enough permissions"
            )
    return user


async def get_scoped_reader(place_source: str,
                            token: str = Depends(oauth2_scheme),
                            db: Session = Depends(get_db)
                            ) -> Type[User]:
    return await get_current_user(SecurityScopes([f"read:{place_source}"]), token, db)


async def get_scoped_writer(place_source: str,
                            token: str = Depends(oauth2_scheme),
                            db: Session = Depends(get_db)) -> Type[User]:
    return await get_current_user(SecurityScopes([f"write:{place_source}"]), token, db)


async def get_user(user: User = Security(get_current_user, scopes=["me"])):
    return user


@auth.get('/users/me')
async def read_user_me(user: User = Depends(get_user)):
    """Check the token functionality.

    Returns:
    user ID and name
    """
    return {"id": user.id, "name": user.name}


@auth.get('/status')
async def api_status(db: Session = Depends(get_db)):
    """Raise API server status with DB connection verification."""
    try:
        db.execute(text("SELECT 1"))
    except:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Internal DB malfunction.")
    return {"status": "ok"}
