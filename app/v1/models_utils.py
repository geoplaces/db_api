from typing import Dict

from sqlalchemy.orm import InstrumentedAttribute

from app.db import Base
from app.models import DejupAssociate, EdenredAssociate, DejPlaces, EdenredPlaces, OsmPlaces, GoPlaces, DejupLocations, \
    EdenredLocations, LocationTable, SourceTable, AssociateTable, TargetTable
from app.v1.schemas import SourceEnum, TargetEnum

associate_tables: Dict[SourceEnum, AssociateTable] = {
    SourceEnum.dejup: DejupAssociate,
    SourceEnum.edenred: EdenredAssociate
}

edenred_link_column: Dict[TargetEnum, InstrumentedAttribute] = {
    TargetEnum.osm: EdenredAssociate.osm_id,
    TargetEnum.google: EdenredAssociate.gomaps_id
}

dejup_link_column: Dict[TargetEnum, InstrumentedAttribute] = {
    TargetEnum.osm: DejupAssociate.osm_id,
    TargetEnum.google: DejupAssociate.gomaps_id
}

link_column: Dict[SourceEnum, Dict[TargetEnum, InstrumentedAttribute]] = {
    SourceEnum.dejup: dejup_link_column,
    SourceEnum.edenred: edenred_link_column
}

link_source_id: Dict[SourceEnum, InstrumentedAttribute] = {
    SourceEnum.dejup: DejupAssociate.dejup_id,
    SourceEnum.edenred: EdenredAssociate.edenred_id
}

source_tables: Dict[SourceEnum, SourceTable] = {
    SourceEnum.dejup: DejPlaces,
    SourceEnum.edenred: EdenredPlaces
}

target_tables: Dict[TargetEnum, TargetTable] = {
    TargetEnum.osm: OsmPlaces,
    TargetEnum.google: GoPlaces
}

location_tables: Dict[SourceEnum, LocationTable] = {
    SourceEnum.dejup: DejupLocations,
    SourceEnum.edenred: EdenredLocations
}


def to_dict(model: Base) -> Dict:
    result = {}
    serialised = model.__dict__
    for key in serialised.keys():
        if key[0] != '_':
            result[key] = serialised[key]

    return result
