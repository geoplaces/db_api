from datetime import datetime, timedelta
from typing import Dict

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy import delete, update
from sqlalchemy.dialects.postgresql import insert
from sqlalchemy.orm import Session
from starlette import status

from app.db import get_db
from app.models import LocationTable
from app.v1.auth import get_scoped_reader, oauth2_scheme, get_scoped_writer
from app.v1.locations_utils import get_location_table, get_location_query, create_location_record
from app.v1.models_utils import to_dict
from app.v1.schemas import SourceEnum, LocationPatch

locations = APIRouter(
    prefix="/locations",
    tags=["Locations"],
    responses={404: {"description": "Not found"}},
)


@locations.get('/{loc_source}', status_code=status.HTTP_200_OK)
async def get_indecies(loc_source: SourceEnum,
                       query: str = '',
                       outdated: int = 0,
                       token: str = Depends(oauth2_scheme),
                       db: Session = Depends(get_db),
                       loc_table: LocationTable = Depends(get_location_table),
                       ):
    """
    Get locations from the zip code table

    Parameters:
    query (str): outdated/not_updated
    outdated(int): indecies updated > N days ago. Requires query == 'outdated'
    """
    await get_scoped_reader(f'{loc_source}_locations', token, db)
    db_query = get_location_query(loc_source)
    if query == 'not_updated':
        db_indecies = db.execute(db_query.where(
            loc_table.updated_at.is_(None))).mappings().all()
    elif query == 'outdated':
        limit = datetime.now() - timedelta(days=outdated)
        db_indecies = db.execute(db_query.where(
            loc_table.updated_at < limit)).mappings().all()
    elif query == '':
        db_indecies = db.execute(db_query).mappings().all()
    else:
        raise HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail=f'Unsupported location query {query}'
        )
    return db_indecies


@locations.patch('/{loc_source}/{location_id}', status_code=status.HTTP_200_OK)
async def update_indecies(loc_source: SourceEnum,
                          location_id: int,
                          put_location: LocationPatch,
                          db: Session = Depends(get_db),
                          token: str = Depends(oauth2_scheme),
                          loc_table: LocationTable = Depends(get_location_table)
                          ):
    """Update the locations update date."""
    await get_scoped_writer(f'{loc_source}_locations', token, db)
    values = {loc_table.updated_at.name: put_location.updated_at}
    rows = db.execute(update(loc_table).values(values).where(loc_table.index == location_id)).rowcount
    if rows == 0:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Location {location_id} not found"
        )
    db.commit()


@locations.post('/{loc_source}', status_code=status.HTTP_201_CREATED)
async def upload_indecies(loc_source: SourceEnum,
                          post_location: Dict,
                          db: Session = Depends(get_db),
                          token: str = Depends(oauth2_scheme),
                          loc_table: LocationTable = Depends(get_location_table)
                          ):
    """Post new locations in the DB."""
    await get_scoped_writer(f'{loc_source}_locations', token, db)
    db_index = create_location_record(loc_source, post_location)
    db.execute(insert(loc_table).values(to_dict(db_index)).on_conflict_do_nothing())
    db.commit()


@locations.delete('/{loc_source}/{location_id}', status_code=status.HTTP_204_NO_CONTENT)
async def delete_index(loc_source: SourceEnum,
                       location_id: int,
                       db: Session = Depends(get_db),
                       token: str = Depends(oauth2_scheme),
                       loc_table: LocationTable = Depends(get_location_table)
                       ):
    """Delete the location by key."""
    await get_scoped_writer(f'{loc_source}_locations', token, db)
    rows = db.execute(delete(loc_table).where(loc_table.index == location_id)).rowcount
    if rows == 0:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Location {location_id} not found"
        )
    db.commit()
