from typing import Dict, Any

from fastapi import HTTPException
from sqlalchemy import select, Select
from starlette import status

from app.models import LocationTable, DejupLocations, EdenredLocations
from app.v1.models_utils import location_tables
from app.v1.schemas import SourceEnum, convert_with_throw, DejupLocationPost, \
    EdenredLocationPost


def get_location_table(loc_source: SourceEnum) -> LocationTable:
    table = location_tables.get(loc_source)
    if table is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f'Unknown index source {loc_source}'
        )
    return table


def create_location_record(loc_source: str, ind: Dict) -> LocationTable:
    """Creates an index DB record based on index type."""
    if loc_source == SourceEnum.dejup:
        record: DejupLocationPost = convert_with_throw(ind, DejupLocationPost)
        return DejupLocations(index=record.zipcode)
    if loc_source == SourceEnum.edenred:
        record: EdenredLocationPost = convert_with_throw(ind, EdenredLocationPost)
        return EdenredLocations(lat=record.lat,
                                lon=record.long,
                                radius=record.radius)


def get_location_query(loc_source: SourceEnum) -> Select[Any]:
    """Build a DB query with return values based on an index type."""
    if loc_source == SourceEnum.dejup:
        return select(DejupLocations.index)
    if loc_source == SourceEnum.edenred:
        return select(EdenredLocations.index.label('index'),
                      EdenredLocations.lat.label('lat'),
                      EdenredLocations.lon.label('lon'),
                      EdenredLocations.radius.label('radius'))
