from typing import List

from fastapi import HTTPException, status
from sqlalchemy.dialects.postgresql import insert
from sqlalchemy.orm import Session

from app.models import GoPlaces, OsmPlaces, DejPlaces, EdenredPlaces, AnyPlaceTable, SourceTable, AssociateTable
from app.point_orm import Coordinate
from app.v1.models_utils import target_tables, link_source_id, source_tables
from app.v1.schemas import schema_validator, GooglePlace, OsmPlace, DejPlace, EdenredPlace, TargetEnum, SourceEnum, \
    PlaceEnum


def get_place_table(place_source: PlaceEnum) -> AnyPlaceTable:
    table = source_tables.get(place_source)
    if table is None:
        table = target_tables.get(place_source)

    if table is None:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Place source `{place_source}` is unknown"
        )
    return table


def create_associate_records(places_id: List[int | str],
                             place_source: SourceTable,
                             associate_table: AssociateTable,
                             db: Session):
    id_col = link_source_id[place_source]
    to_insert = [{id_col.name: place_id} for place_id in places_id]
    if len(to_insert) != 0:
        db.execute(insert(associate_table).values(to_insert).on_conflict_do_nothing())


@schema_validator(TargetEnum.google)
def google_record_creator(place: GooglePlace) -> GoPlaces:
    """Insert a new place and link it if possible."""
    return GoPlaces(id=place.id,
                    name=place.name,
                    address=place.address,
                    location=Coordinate(place.lat, place.long),
                    updated_at=place.updated_at
                    )


@schema_validator(TargetEnum.osm)
def osm_record_creator(place: OsmPlace) -> OsmPlaces:
    """Transform a schema to DB record."""
    return OsmPlaces(id=place.id,
                     osm_type=place.osm_type,
                     name=place.name,
                     house_number=place.house_number,
                     road=place.road,
                     town=place.town,
                     postcode=place.postcode,
                     location=Coordinate(place.lat, place.long),
                     updated_at=place.updated_at
                     )


@schema_validator(SourceEnum.edenred)
def edenred_record_creator(place: EdenredPlace):
    """Transform a schema to DB record."""
    return EdenredPlaces(id=place.id,
                         name=place.name,
                         address=place.address,
                         zipcode=place.zipcode,
                         city=place.city,
                         location=Coordinate(place.lat, place.long),
                         categoryName=place.categoryName,
                         updated_at=place.updated_at
                         )


@schema_validator(SourceEnum.dejup)
def dejup_record_creator(place: DejPlace):
    """Transform a schema to DB record."""
    return DejPlaces(id=place.id,
                     link=place.link,
                     name=place.name,
                     address=place.address,
                     address2=place.address2,
                     zipcode=place.zipcode,
                     city=place.city,
                     x=place.x,
                     y=place.y,
                     accepteCheque=place.accepteCheque,
                     accepteCarte3C=place.accepteCarte3C,
                     accepteCarte4C=place.accepteCarte4C,
                     accepteCarte4C2=place.accepteCarte4C2,
                     accepteCarteHybride=place.accepteCarteHybride,
                     upPlus=place.upPlus,
                     categoryName=place.categoryName,
                     updated_at=place.updated_at
                     )


places_record_creators = {
    "dejup": dejup_record_creator,
    "edenred": edenred_record_creator,
    "osm": osm_record_creator,
    "google": google_record_creator
}
