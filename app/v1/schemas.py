"""API schemas."""

from datetime import datetime
from enum import Enum
from typing import Dict, Optional

from fastapi import HTTPException, status
from pydantic import BaseModel
from pydantic.error_wrappers import ValidationError


class EdenredLocationPost(BaseModel):
    """Edenred location create schema"""
    lat: float
    long: float
    radius: float


class DejupLocationPost(BaseModel):
    """Dejup location create schema."""
    zipcode: int


class LocationPatch(BaseModel):
    """Update location schema."""
    updated_at: datetime


class SourceEnum(str, Enum):
    dejup = "dejup"
    edenred = "edenred"


class TargetEnum(str, Enum):
    osm = "osm"
    google = "google"


PlaceEnum = SourceEnum | TargetEnum


class AssociateQueryEnum(str, Enum):
    no_osm = "no_osm"
    no_google = "no_google"
    osm_dummy = "osm_dummy"
    osm_dummy_no_google = "osm_dummy_no_google"
    osm_way = "osm_way"


class DejPlace(BaseModel):
    """Schema for a dejup place."""
    link: str
    name: str
    address: str
    address2: Optional[str] = None
    zipcode: int
    city: str
    updated_at: datetime
    id: int
    x: Optional[int] = None
    y: Optional[int] = None
    accepteCheque: Optional[bool] = False
    accepteCarte3C: Optional[bool] = False
    accepteCarte4C: Optional[bool] = False
    accepteCarte4C2: Optional[bool] = False
    accepteCarteHybride: Optional[bool] = False
    upPlus: Optional[bool] = False
    categoryName: Optional[str] = None


class EdenredPlace(BaseModel):
    """Schema for an Edenred place."""
    id: str
    name: str
    address: str
    zipcode: int
    city: str
    lat: float
    long: float
    updated_at: datetime
    categoryName: Optional[str] = None


class OsmPlace(BaseModel):
    """Schema for OSM places."""
    id: str
    osm_type: str
    name: Optional[str] = None
    house_number: Optional[str] = None
    road: Optional[str] = None
    town: Optional[str] = None
    postcode: Optional[str] = None
    lat: float
    long: float
    updated_at: datetime


class GooglePlace(BaseModel):
    """Schema for Google places."""
    id: str
    name: str
    address: str
    lat: float
    long: float
    updated_at: datetime


class Associates(BaseModel):
    """Schema for changing association links."""
    osm_id: Optional[str] = None
    gomaps_id: Optional[str] = None


schema_dict = {
    SourceEnum.dejup: DejPlace,
    SourceEnum.edenred: EdenredPlace,
    TargetEnum.osm: OsmPlace,
    TargetEnum.google: GooglePlace
}

ValidatedSchema = DejPlace | EdenredPlace | OsmPlace | GooglePlace


def convert_with_throw(source: Dict, target):
    """
    Converts any Dict into Schema model

    Throws an HTTP exception if schema is not valid
    """
    try:
        place = target(**source)
    except ValidationError as err:
        raise HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail=f"Invalid data format: {err}"
        )
    return place


def schema_validator(place_source: PlaceEnum):
    def decorator(func):
        def wrapper(place: Dict, *args, **kwargs) -> ValidatedSchema:
            place_schema: ValidatedSchema = convert_with_throw(place, schema_dict[place_source])
            return func(place_schema, *args, **kwargs)

        return wrapper

    return decorator
