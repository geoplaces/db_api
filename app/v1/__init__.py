from fastapi import APIRouter

from app.v1 import places, associate, auth, locations


def create_router_v1() -> APIRouter:
    router = APIRouter(
        prefix="/v1",
        tags=["v1"],
        responses={404: {"description": "Not found"}},
    )
    router.include_router(auth.auth)
    router.include_router(locations.locations)
    router.include_router(associate.associate)
    router.include_router(associate.own_coordinates)
    router.include_router(places.place)
    return router
