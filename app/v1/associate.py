from fastapi import APIRouter, Depends, HTTPException, Security, status
from sqlalchemy import and_, literal_column, or_, select, String, text
from sqlalchemy.orm import Session

from app.db import get_db
from app.models import AssociateTable, DejPlaces, EdenredPlaces, GoPlaces, OsmPlaces, SourceTable, User
from app.v1.auth import get_current_user
from app.v1.models_utils import target_tables
from app.v1.schemas import AssociateQueryEnum, Associates, SourceEnum, TargetEnum
from .associate_utils import add_dummy_link, coordinates_to_xy, DUMMY_ID, get_associate_table, get_source_table, \
    get_within_coords_utils, xy_to_coordinates


def get_associate_reader(user: User = Security(get_current_user, scopes=["read:associate"])):
    return user


def get_associate_writer(user: User = Security(get_current_user, scopes=["write:associate"])):
    return user


associate = APIRouter(
        prefix="/associations",
        tags=["Associations"],
        responses={404: {"description": "Not found"}},
)


@associate.patch("/{associate_source}/{associate_id}", status_code=status.HTTP_200_OK)
def update_association(associate_id: int,
                       links: Associates,
                       db: Session = Depends(get_db),
                       associate_table: AssociateTable = Depends(get_associate_table),
                       _user: User = Depends(get_associate_writer)):
    """Update the association between source and target."""
    if links.osm_id is None and links.gomaps_id is None:
        raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="No links are provided"
        )
    associate_record = db.query(associate_table
                                ).filter_by(id=associate_id).first()
    if not associate_record:
        raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"No associate record {associate_id}"
        )

    if links.osm_id is not None:
        osm_record = db.execute(select(OsmPlaces).where(OsmPlaces.id == links.osm_id)).one_or_none()
        if osm_record is None:
            raise HTTPException(
                    status_code=status.HTTP_400_BAD_REQUEST,
                    detail=f"No OSM record {links.osm_id}"
            )
        associate_record.osm_id = links.osm_id

    if links.gomaps_id is not None:
        google_record = db.execute(select(GoPlaces).where(GoPlaces.id == links.gomaps_id)).one_or_none()
        if google_record is None:
            raise HTTPException(
                    status_code=status.HTTP_400_BAD_REQUEST,
                    detail=f"No Google record {links.osm_id}"
            )
        associate_record.gomaps_id = links.gomaps_id

    db.commit()


@associate.post("/{associate_source}/{associate_id}/dummy/{target}", status_code=status.HTTP_200_OK)
def link_dummy_target(associate_source: SourceEnum,
                      associate_id: int,
                      target: TargetEnum,
                      db: Session = Depends(get_db),
                      associate_table: AssociateTable = Depends(get_associate_table),
                      _user: User = Depends(get_associate_writer)):
    """Post a dummy record for an association."""
    associate_record = db.query(associate_table
                                ).filter_by(id=associate_id).first()
    if not associate_record:
        raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"No associate record {associate_id}"
        )
    if target not in target_tables:
        raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail=f"Unknown target {target}"
        )
    add_dummy_link(db, target, associate_source, associate_id)


@associate.get('/{associate_source}')
def query_places(query: AssociateQueryEnum,
                 start_id: int = -1,
                 limit: int = 100,
                 db: Session = Depends(get_db),
                 associate_table: AssociateTable = Depends(get_associate_table),
                 source_table: SourceTable = Depends(get_source_table),
                 _user: User = Depends(get_associate_reader)):
    """Get source places with query about location quality.
    E.g. no OSM location or OWM 'way' location only

    Supports pagination with default step of 100 and order by record id.

    Parameters:
    query(str): no_osm/no_google/osm_dummy/osm_dummy_no_google/osm_way
    start_id (int): The start id of the association record
    limit (int): The limit of the number of the returned places
    """
    qu = select(
            source_table.address.label('address'),
            source_table.zipcode.label('zipcode'),
            source_table.city.label('city'),
            associate_table.id.label('associate_id')
    ).join(associate_table)

    if query == AssociateQueryEnum.no_osm:
        qu = qu.where(and_(associate_table.id > start_id,
                           associate_table.osm_id.is_(None)))
    elif query == AssociateQueryEnum.no_google:
        qu = qu.where(and_(associate_table.id > start_id,
                           associate_table.gomaps_id.is_(None)))
    elif query == AssociateQueryEnum.osm_dummy:
        qu = qu.where(and_(associate_table.id > start_id,
                           associate_table.osm_id == DUMMY_ID))
    elif query == AssociateQueryEnum.osm_dummy_no_google:
        qu = qu.where(and_(associate_table.id > start_id,
                           associate_table.osm_id == DUMMY_ID,
                           associate_table.gomaps_id.is_(None)))
    elif query == AssociateQueryEnum.osm_way:
        qu = qu.where(and_(associate_table.id > start_id,
                           or_(OsmPlaces.osm_type == 'way',
                               OsmPlaces.osm_type == 'relation'
                               ),
                           associate_table.gomaps_id.is_(None)))
        qu = qu.join(OsmPlaces)
    else:
        raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail=f'Wrong query value: {query}'
        )

    qu = qu.order_by(associate_table.id).limit(limit)
    return db.execute(qu).mappings().all()


# FRONT END
LOCATION_BIND = "location <@ box'((:x1, :y1), (:x2,:y2))'"


@associate.get('/{associate_source}/within_coords')
def associated_coords(n: float,
                      s: float,
                      w: float,
                      e: float,
                      limit: int = 100,
                      db: Session = Depends(get_db),
                      associate_table: AssociateTable = Depends(get_associate_table),
                      source_table: SourceTable = Depends(get_source_table)):
    """
    Read coordinates from joined source and target databases.
    E.g. Edenred objects complemented with OSM and Google Maps data.
    """
    no_google_info = or_(associate_table.gomaps_id.is_(None),
                         associate_table.gomaps_id == DUMMY_ID
                         )
    osm_within_coords = (text(f"{OsmPlaces.__tablename__}.{LOCATION_BIND}")
                         .bindparams(x1=s, y1=w, x2=n, y2=e))
    select_query = get_within_coords_utils(source_table, OsmPlaces, associate_table).where(
            and_(no_google_info,
                 osm_within_coords
                 )
    )
    places = list(db.execute(select_query).mappings().all())
    google_within_coords = (text(f"{GoPlaces.__tablename__}.{LOCATION_BIND}")
                            .bindparams(x1=s, y1=w, x2=n, y2=e))
    select_query = get_within_coords_utils(source_table, GoPlaces, associate_table).where(
            google_within_coords
    )
    places_google = db.execute(select_query).mappings().all()

    places.extend(places_google)

    return {
        'total': len(places),
        'limit': limit,
        'data' : places[:limit]
    }


own_coordinates = APIRouter(
        prefix="",
        tags=["Coordinates"],
        responses={404: {"description": "Not found"}},
)


@own_coordinates.get('/dejup/places_coords')
async def dejup_coord(n: float,
                      s: float,
                      w: float,
                      e: float,
                      limit: int = 100,
                      db: Session = Depends(get_db)):
    """Returns places within the Dejup native coordinates."""
    x_max, y_max = coordinates_to_xy(e, n)
    x_min, y_min = coordinates_to_xy(w, s)
    query = select(DejPlaces.x.label('x'),
                   DejPlaces.y.label('y'),
                   DejPlaces.name.label('name'),
                   DejPlaces.id.label('id'),
                   DejPlaces.categoryName.label('cat'),
                   DejPlaces.address.label('address'),
                   DejPlaces.city.label('city')).filter(and_(DejPlaces.x > x_min,
                                                             DejPlaces.x < x_max,
                                                             DejPlaces.y > y_min,
                                                             DejPlaces.y < y_max,
                                                             ))
    places = db.execute(query).mappings().all()

    result = {
        'total': len(places),
        'limit': limit,
        'data' : []
    }
    for place in places[:limit]:
        result['data'].append(dict(place))
        result['data'][-1]['lon'], result['data'][-1]['lat'] = xy_to_coordinates(place['x'], place['y'])
        result['data'][-1].pop('x')
        result['data'][-1].pop('y')

    return result


@own_coordinates.get('/edenred/places_coords')
async def places_coord(n: float,
                       s: float,
                       w: float,
                       e: float,
                       limit: int = 100,
                       db: Session = Depends(get_db)):
    """Returns places within the Edenred native coordinates."""
    query = select(literal_column("location[0]", String).label('lat'),
                   literal_column("location[1]", String).label('lon'),
                   EdenredPlaces.name.label('name'),
                   EdenredPlaces.id.label('id'),
                   EdenredPlaces.categoryName.label('cat'),
                   EdenredPlaces.address.label('address'),
                   EdenredPlaces.city.label('city')
                   ).where(
            text(LOCATION_BIND).bindparams(x1=s, y1=w, x2=n, y2=e)
    )
    places = db.execute(query).mappings().all()

    return {
        'total': len(places),
        'limit': limit,
        'data' : places[:limit]
    }
