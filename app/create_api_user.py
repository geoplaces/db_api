import string
from random import choice

import click
from sqlalchemy import insert
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import Session
from werkzeug.security import generate_password_hash

from db import get_db
from models import User


def gen_salt(length: int) -> str:
    letters = string.ascii_lowercase
    return ''.join(choice(letters) for _ in range(length))


@click.command
@click.option('--username', '-u', type=str, prompt='Your username')
@click.option('--password', '-p', type=str, prompt='Your password')
def main(username: str, password: str):
    db: Session = next(get_db())

    salt = gen_salt(16)
    encoded_password = generate_password_hash(password + salt)

    try:
        db.execute(insert(User).values(name=username, pasw=encoded_password, salt=salt))
        db.commit()
    except IntegrityError:
        print('User with this name already exists')


if __name__ == "__main__":
    main()
