"""Add point table

Revision ID: cdc2cbedb6bd
Revises: 139f39a45199
Create Date: 2023-11-28 19:58:36.055382

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy import text
from sqlalchemy.dialects import postgresql
import point_orm

# revision identifiers, used by Alembic.
revision = 'cdc2cbedb6bd'
down_revision = '139f39a45199'
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    conn = op.get_bind()
    op.add_column('edenred_places', sa.Column('location', point_orm.LatLngType(), nullable=True))
    conn.execute(text("UPDATE edenred_places SET location=point( lat , lon );"))
    conn.execute(text("CREATE INDEX location_edenred_index ON edenred_places USING gist (location);"))
    op.drop_column('edenred_places', 'lat')
    op.drop_column('edenred_places', 'lon')
    op.add_column('go_places', sa.Column('location', point_orm.LatLngType(), nullable=True))
    conn.execute(text("UPDATE go_places SET location=point( lat , lon );"))
    conn.execute(text("CREATE INDEX location_go_index ON go_places USING gist (location);"))
    op.drop_column('go_places', 'lat')
    op.drop_column('go_places', 'lon')
    op.add_column('osm_places', sa.Column('location', point_orm.LatLngType(), nullable=True))
    conn.execute(text("UPDATE osm_places SET location=point( lat , lon );"))
    conn.execute(text("CREATE INDEX location_osm_index ON osm_places USING gist (location);"))
    op.drop_column('osm_places', 'lon')
    op.drop_column('osm_places', 'lat')
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    conn = op.get_bind()
    op.add_column('osm_places', sa.Column('lat', postgresql.DOUBLE_PRECISION(precision=53), autoincrement=False, nullable=True))
    op.add_column('osm_places', sa.Column('lon', postgresql.DOUBLE_PRECISION(precision=53), autoincrement=False, nullable=True))
    conn.execute("UPDATE osm_places SET lat=location[0], lon=location[1];")
    op.drop_index('location_osm_index', table_name='osm_places')
    op.drop_column('osm_places', 'location')
    op.add_column('go_places', sa.Column('lon', postgresql.DOUBLE_PRECISION(precision=53), autoincrement=False, nullable=True))
    op.add_column('go_places', sa.Column('lat', postgresql.DOUBLE_PRECISION(precision=53), autoincrement=False, nullable=True))
    conn.execute("UPDATE go_places SET lat=location[0], lon=location[1];")
    op.drop_index('location_go_index', table_name='go_places')
    op.drop_column('go_places', 'location')
    op.add_column('edenred_places', sa.Column('lon', postgresql.DOUBLE_PRECISION(precision=53), autoincrement=False, nullable=True))
    op.add_column('edenred_places', sa.Column('lat', postgresql.DOUBLE_PRECISION(precision=53), autoincrement=False, nullable=True))
    conn.execute("UPDATE edenred_places SET lat=location[0], lon=location[1];")
    op.drop_index('location_edenred_index', table_name='edenred_places')
    op.drop_column('edenred_places', 'location')
    # ### end Alembic commands ###
