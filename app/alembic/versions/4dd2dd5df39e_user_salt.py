"""user salt

Revision ID: 4dd2dd5df39e
Revises: 72bb1ac72462
Create Date: 2024-04-26 14:33:36.410893

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '4dd2dd5df39e'
down_revision = '72bb1ac72462'
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('users', sa.Column('salt', sa.String(), nullable=False))
    op.create_unique_constraint(None, 'users', ['name'])
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, 'users', type_='unique')
    op.drop_column('users', 'salt')
    # ### end Alembic commands ###
