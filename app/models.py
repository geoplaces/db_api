"""DB tables."""
from datetime import datetime

from sqlalchemy import Integer, String, DateTime, ForeignKey, Boolean
from sqlalchemy.orm import relationship, Mapped, mapped_column

# Dirty hack to make both alembic and uvicorn work
try:
    from .db import Base
    from .point_orm import LatLngType, Coordinate
except ImportError:
    from db import Base
    from point_orm import LatLngType, Coordinate


class User(Base):
    """Users table."""
    __tablename__ = 'users'
    id: Mapped[int] = mapped_column(primary_key=True)
    name: Mapped[str] = mapped_column(String(255), unique=True, nullable=False)
    pasw: Mapped[str] = mapped_column(String, nullable=False)
    salt: Mapped[str] = mapped_column(String, nullable=False)


class DejupAssociate(Base):
    """
    UpDejeuner association
    """
    __tablename__ = 'dejup_associations'
    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    dejup_id: Mapped[int] = mapped_column(Integer, ForeignKey('dej_places.id'), unique=True, nullable=True)
    gomaps_id: Mapped[str] = mapped_column(String, ForeignKey('go_places.id'), nullable=True)
    osm_id: Mapped[str] = mapped_column(String, ForeignKey('osm_places.id'), nullable=True)


class EdenredAssociate(Base):
    """
    Edenred associations
    """
    __tablename__ = 'edenred_associations'
    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    edenred_id: Mapped[str] = mapped_column(String, ForeignKey('edenred_places.id'), unique=True, nullable=True)
    gomaps_id: Mapped[str] = mapped_column(String, ForeignKey('go_places.id'), nullable=True)
    osm_id: Mapped[str] = mapped_column(String, ForeignKey('osm_places.id'), nullable=True)


class DejupLocations(Base):
    """
    Base of indecies of interest.
    To be scanned by Dejeuner scrapper.
    """
    __tablename__ = 'indecies'
    index: Mapped[int] = mapped_column(Integer, primary_key=True)
    updated_at: Mapped[datetime] = mapped_column(DateTime, nullable=True)


class DejPlaces(Base):
    """Places obtained from Dejeuner Up"""
    __tablename__ = 'dej_places'
    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    link: Mapped[str] = mapped_column(String, nullable=True)
    name: Mapped[str]
    address: Mapped[str] = mapped_column(String, nullable=True)
    address2: Mapped[str] = mapped_column(String, nullable=True)
    zipcode: Mapped[int] = mapped_column(Integer, nullable=True)
    city: Mapped[str] = mapped_column(String, nullable=True)
    accepteCheque: Mapped[bool] = mapped_column(Boolean, nullable=True)
    accepteCarte3C: Mapped[bool] = mapped_column(Boolean, nullable=True)
    accepteCarte4C: Mapped[bool] = mapped_column(Boolean, nullable=True)
    accepteCarte4C2: Mapped[bool] = mapped_column(Boolean, nullable=True)
    accepteCarteHybride: Mapped[bool] = mapped_column(Boolean, nullable=True)
    upPlus: Mapped[bool] = mapped_column(Boolean, nullable=True)
    categoryName: Mapped[str] = mapped_column(String, nullable=True)
    x: Mapped[int] = mapped_column(Integer, nullable=True)
    y: Mapped[int] = mapped_column(Integer, nullable=True)
    updated_at: Mapped[datetime] = mapped_column(DateTime)
    associate = relationship('DejupAssociate')


class EdenredLocations(Base):
    """
    Base of places of interest.
    To be scanned by Edenred scrapper.
    """
    __tablename__ = 'edenred_positions'
    index: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    lat: Mapped[float]
    lon: Mapped[float]
    radius: Mapped[float]
    updated_at: Mapped[datetime] = mapped_column(DateTime, nullable=True)


class EdenredPlaces(Base):
    """Places scrapped from Edenred."""
    __tablename__ = 'edenred_places'
    id: Mapped[str] = mapped_column(primary_key=True)
    name: Mapped[str]
    address: Mapped[str] = mapped_column(String, nullable=True)
    city: Mapped[str] = mapped_column(String, nullable=True)
    location: Mapped[Coordinate] = mapped_column(LatLngType, nullable=True)
    zipcode: Mapped[int] = mapped_column(Integer, nullable=True)
    categoryName: Mapped[str] = mapped_column(String, nullable=True)
    updated_at: Mapped[datetime] = mapped_column(DateTime)
    associate = relationship('EdenredAssociate')


class GoPlaces(Base):
    """Places From Google Map"""
    __tablename__ = 'go_places'
    id: Mapped[str] = mapped_column(String, primary_key=True)
    name: Mapped[str] = mapped_column(String, nullable=True)
    address: Mapped[str] = mapped_column(String, nullable=True)
    location: Mapped[Coordinate] = mapped_column(LatLngType, nullable=True)
    updated_at: Mapped[datetime] = mapped_column(DateTime)
    dejup_associate = relationship('DejupAssociate')
    edenred_associate = relationship('EdenredAssociate')


class OsmPlaces(Base):
    """Places from OSM."""
    __tablename__ = 'osm_places'
    id: Mapped[str] = mapped_column(String, primary_key=True)
    osm_type: Mapped[str] = mapped_column(String, nullable=True)
    name: Mapped[str] = mapped_column(String, nullable=True)
    house_number: Mapped[str] = mapped_column(String, nullable=True)
    road: Mapped[str] = mapped_column(String, nullable=True)
    town: Mapped[str] = mapped_column(String, nullable=True)
    postcode: Mapped[str] = mapped_column(String, nullable=True)
    location: Mapped[Coordinate] = mapped_column(LatLngType, nullable=True)
    updated_at: Mapped[datetime] = mapped_column(DateTime)
    dejup_associate = relationship('DejupAssociate')
    edenred_associate = relationship('EdenredAssociate')


SourceTable = EdenredPlaces | DejPlaces
AssociateTable = EdenredAssociate | DejupAssociate
TargetTable = OsmPlaces | GoPlaces
AnyPlaceTable = TargetTable | SourceTable
LocationTable = DejupLocations | EdenredLocations
