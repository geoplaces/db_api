from pathlib import Path

from fastapi import APIRouter, Request
from fastapi.templating import Jinja2Templates

website_router = APIRouter(
        prefix="",
        tags=["website"],
        responses={404: {"description": "Not found"}},
)

templates = Jinja2Templates(directory=f'{Path(__file__).parent}/templates')


@website_router.get('/')
def dejup(request: Request):
    """Main map page."""
    return templates.TemplateResponse(name="main_map.jinja2", request=request)
