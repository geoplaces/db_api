(function () {
    if ('serviceWorker' in navigator) {
        window.addEventListener('load', () => {
            navigator.serviceWorker.register('/static/dist/sw.js')
                .then(function (registration) {
                    console.log('Service Worker Registered');
                    return registration;
                })
                .catch(function (err) {
                    console.error('Unable to register service worker.', err);
                });
            navigator.serviceWorker.ready.then(function (registration) {
                console.log('Service Worker Ready');
            });
        });
    }
})();

var map = L.map('map', {
    zoomControl: false,
    maxZoom: 18
}).setView([48.85, 2.34], 13);
L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
}).addTo(map);
L.control.zoom({
    position: "bottomright"
}).addTo(map);
L.control.scale({
    position: "bottomleft",
    metric: true,
    imperial: false,
    updateWhenIdle: false
}).addTo(map);

L.control.locate({
    position: "bottomright"
}).addTo(map);

var markerGroup = L.layerGroup().addTo(map);

// Spiderfying
const oms = new OverlappingMarkerSpiderfier(map, {keepSpiderfied: true});

// lengthen the spider's leg by 4x
oms.circleFootSeparation = 100;

var DATA;
const updateClick = (event) => {
    document.getElementById("updateButton").textContent = "Updating...";

    document.getElementById("update-err").classList.add("d-none");
    const bounds = map.getBounds();
    const source = window.localStorage['source'];
    let url = "/v1";
    switch (source) {
        case "dejup_complemented":
            url += "/associations/dejup/within_coords";
            break;
        case "edenred_complemented":
            url += "/associations/edenred/within_coords";
            break;
        case "edenred_native":
            url += "/edenred/places_coords";
            break;
        case "dejup_native":
            url += "/dejup/places_coords";
            break;
        default:
            url += "/associations/dejup/within_coords";
    }
    const coords = {
        'n': bounds.getNorth(),
        's': bounds.getSouth(),
        'w': bounds.getWest(),
        'e': bounds.getEast(),
        'source': window.localStorage['source'],
        'limit': window.localStorage["limit"]
    }
    $.get(url, coords)
        .done((date) => {
            markerGroup.clearLayers();
            oms.unspiderfy();
            DATA = date["data"];
            if (date["data"].length < date["total"]) {
                $("#markers-description").text("Show " + date["data"].length + " out of " + date["total"] + " places.");
                document.getElementById("markers-description").classList.remove("d-none");
            } else {
                document.getElementById("markers-description").classList.add("d-none");
            }
            if (date["data"].length === 0) {
                $("#markers-description").text("Nothing found in the region!");
                document.getElementById("markers-description").classList.remove("d-none");
            }
            date["data"].forEach((place) => {
                var marker = L.marker([place['lat'], place['lon']]);
                let popText = place["name"];
                if (window.localStorage.getItem("source") === "dejup_native" || window.localStorage.getItem("source") === "dejup_complemented") {
                    let url = "https://ou-cheque-dejeuner.fr/restaurateur/" + place["city"].replaceAll(' ', '-') + "-" + place["name"].replaceAll(' ', '-') + "-P"
                    for (let i = 0; i < 7 - String(place["id"]).length; i++) {
                        url += "0";
                    }
                    url += place["id"];
                    popText = "<a href=\"" + url + "\" target=\"_blank\">" + place["name"] + "</a>";
                }

                popText += "<p class='.text-secondary'>" + place["address"];
                if (place["cat"] !== undefined) {
                    popText += "<br>" + place["cat"] + "</p>";
                }
                marker.bindPopup(popText);
                // add to map
                markerGroup.addLayer(marker);
                // do spyderfy
                oms.addMarker(marker);
                if (!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                    marker.on('mouseover', (ev) => {
                        marker.openPopup();
                    });
                }
            });
            document.getElementById("updateButton").style.visibility = "hidden";
        }).fail((xhr, _textStatus) => {
        console.error("Error api call with status " + xhr.status);
        document.getElementById("markers-description").classList.add("d-none");
        document.getElementById("update-err").classList.remove("d-none");
    });
    document.getElementById("updateButton").textContent = "Update";
}

/**
 * On the end of the map move
 */
map.on("moveend", () => {
    if (window.localStorage["autoUpdate"] === "true") {
        document.getElementById("updateButton").textContent = "Updating...";
        updateClick();
    } else {
        document.getElementById("updateButton").style.visibility = "visible";
    }
});

const listRender = (event) => {
    const parent = document.getElementById("listBody")
    parent.innerHTML = "";
    let i = 0;
    DATA.forEach((place) => {
        let newResto = document.createElement("div");
        newResto.classList.add("py-1");
        newResto.classList.add("px-2");
        if (i % 2 === 0) {
            newResto.classList.add("dimmed");
        }
        ++i;
        let name = document.createElement("div");
        name.textContent = place["name"];
        newResto.appendChild(name);

        parent.appendChild(newResto);
    })
}

const saveSettings = () => {
    // sourceSelector
    let select = document.getElementById("sourceSelector");
    window.localStorage["source"] = select[select.selectedIndex].value;
    var myModalEl = document.querySelector('#settingsModal')
    var modal = bootstrap.Modal.getOrCreateInstance(myModalEl)
    // autoUpdate
    window.localStorage["autoUpdate"] = document.getElementById("autoUpdateSwitch").checked;
    // limit
    window.localStorage["limit"] = document.getElementById("placeRange").value;

    modal.hide();
    updateClick();
}

const initSettings = () => {
    if (window.localStorage.getItem("source") === null) {
        window.localStorage["source"] = "dejup_complemented";
    }
    if (window.localStorage.getItem("autoUpdate") === null) {
        window.localStorage["autoUpdate"] = false;
    }
    if (window.localStorage.getItem("limit") === null) {
        window.localStorage["limit"] = 100;
    }
}

const defineSettings = () => {
    let select = document.getElementById("sourceSelector");
    select.value = window.localStorage["source"];

    document.getElementById("autoUpdateSwitch").checked = window.localStorage["autoUpdate"] === "true";

    document.getElementById("placeRange").value = window.localStorage.getItem("limit");
    document.getElementById("placesNum").textContent = document.getElementById("placeRange").value;
}

document.getElementById("placeRange").onchange = () => {
    document.getElementById("placesNum").textContent = document.getElementById("placeRange").value;
}

window.onload = () => {
    initSettings();
    defineSettings();

    setTimeout(() => {
        updateClick();
    }, 0);
}