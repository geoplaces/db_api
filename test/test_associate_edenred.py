from json import dumps

import pytest

from test.conftest import ASSOCIATE_EDENRED_URL
from test.test_edenred import add_edenred_place
from test.test_google import SAMPLE_GOOGLE_PLACE, GOOGLE_PLACES_BATCH_URL
from test.test_osm import SAMPLE_OSM_PLACE, add_osm_place, OSM_PLACES_BATCH_URL


@pytest.fixture(scope='function')
def add_linked_osm(client, associate_headers, osm_headers, add_edenred_place):
    resp = client.get(f'{ASSOCIATE_EDENRED_URL}?query=no_osm', headers=associate_headers)
    link = resp.json()[0]['associate_id']

    place = [SAMPLE_OSM_PLACE.copy()]
    resp = client.post(f'{OSM_PLACES_BATCH_URL}', headers=osm_headers, content=dumps(place))
    assert resp.status_code == 201
    resp = client.patch(f'{ASSOCIATE_EDENRED_URL}/{link}', headers=associate_headers,
                        content=dumps({'osm_id': SAMPLE_OSM_PLACE["id"]}))
    assert resp.status_code == 200


def test_edenred_no_osm(client, add_edenred_place, associate_headers, osm_headers):
    # test with 1 added dejup place --> should be returned
    resp = client.get(f'{ASSOCIATE_EDENRED_URL}?query=no_osm', headers=associate_headers)
    assert resp.status_code == 200
    assert len(resp.json()) == 1
    link = resp.json()[0]['associate_id']

    # add associated OSM place --> empty list should be returned
    resp = client.put(f'{OSM_PLACES_BATCH_URL}', headers=osm_headers, content=dumps([SAMPLE_OSM_PLACE.copy()]))
    assert resp.status_code == 200

    resp = client.patch(f'{ASSOCIATE_EDENRED_URL}/{link}', headers=associate_headers,
                        content=dumps({'osm_id': SAMPLE_OSM_PLACE["id"]}))
    assert resp.status_code == 200

    resp = client.get(f'{ASSOCIATE_EDENRED_URL}?query=no_osm', headers=associate_headers)
    assert resp.status_code == 200
    assert len(resp.json()) == 0


def test_edenred_no_google(client, add_edenred_place, google_headers, associate_headers):
    # test with 1 added dejup place --> should be returned
    resp = client.get(f'{ASSOCIATE_EDENRED_URL}?query=no_google', headers=associate_headers)
    assert resp.status_code == 200
    assert len(resp.json()) == 1
    link = resp.json()[0]['associate_id']

    # add associated Google place --> empty list should be returned
    google_place = SAMPLE_GOOGLE_PLACE.copy()
    resp = client.post(f'{GOOGLE_PLACES_BATCH_URL}', headers=google_headers, content=dumps([google_place]))
    assert resp.status_code == 201

    resp = client.patch(f'{ASSOCIATE_EDENRED_URL}/{link}', headers=associate_headers,
                        content=dumps({'gomaps_id': SAMPLE_GOOGLE_PLACE["id"]}))
    assert resp.status_code == 200

    resp = client.get(f'{ASSOCIATE_EDENRED_URL}?query=no_google', headers=associate_headers)
    assert resp.status_code == 200
    assert len(resp.json()) == 0


def test_edenred_osm_dummy(client, token, add_edenred_place):
    # add dejup entry w/o link to OSM
    headers_read = {'Authorization': 'Bearer ' + token('read:associate')}
    headers_write = {'Authorization': 'Bearer ' + token('write:associate')}
    resp = client.get(f'{ASSOCIATE_EDENRED_URL}?query=no_osm', headers=headers_read)
    associate_id = resp.json()[0]["associate_id"]

    # add dummy
    resp = client.post(f'{ASSOCIATE_EDENRED_URL}/{associate_id}/dummy/osm', headers=headers_write)
    assert resp.status_code == 200

    resp = client.get(f'{ASSOCIATE_EDENRED_URL}?query=osm_dummy', headers=headers_read)
    assert resp.status_code == 200
    assert len(resp.json()) > 0

    resp = client.get(f'{ASSOCIATE_EDENRED_URL}?query=osm_dummy_no_google', headers=headers_read)
    assert resp.status_code == 200
    assert len(resp.json()) > 0

    resp = client.post(f'{ASSOCIATE_EDENRED_URL}/{associate_id}/dummy/google', headers=headers_write)
    assert resp.status_code == 200

    resp = client.get(f'{ASSOCIATE_EDENRED_URL}?query=osm_dummy', headers=headers_read)
    assert resp.status_code == 200
    assert len(resp.json()) > 0

    resp = client.get(f'{ASSOCIATE_EDENRED_URL}?query=osm_dummy_no_google', headers=headers_read)
    assert resp.status_code == 200
    assert len(resp.json()) == 0


# FRONT END
def test_edenred_coords(client, add_linked_osm):
    params = {'n': SAMPLE_OSM_PLACE['lat'] + 1,
              's': SAMPLE_OSM_PLACE['lat'] - 1,
              'e': SAMPLE_OSM_PLACE['long'] + 1,
              'w': SAMPLE_OSM_PLACE['long'] - 1
              }
    resp = client.get(f'{ASSOCIATE_EDENRED_URL}/within_coords', params=params)

    assert resp.status_code == 200
    assert resp.json().get('total') > 0
    assert len(resp.json().get('data')) > 0

    params = {'n': SAMPLE_OSM_PLACE['lat'] + 2,
              's': SAMPLE_OSM_PLACE['lat'] + 1,
              'e': SAMPLE_OSM_PLACE['long'] + 1,
              'w': SAMPLE_OSM_PLACE['long'] - 1
              }
    resp = client.get(f'{ASSOCIATE_EDENRED_URL}/within_coords', params=params)

    assert resp.status_code == 200
    assert resp.json().get('total') == 0
    assert len(resp.json().get('data')) == 0


def test_edenred_osm_way(client, osm_headers, associate_headers, add_osm_place, add_edenred_place):
    resp = client.get(f'{ASSOCIATE_EDENRED_URL}?query=osm_way', headers=associate_headers)
    assert resp.status_code == 200
    assert len(resp.json()) == 0

    resp = client.get(f'{ASSOCIATE_EDENRED_URL}?query=no_osm', headers=associate_headers)
    associate_id = resp.json()[0]["associate_id"]

    new_place = [SAMPLE_OSM_PLACE.copy()]
    new_place[0]['osm_type'] = 'way'
    resp = client.put(f'{OSM_PLACES_BATCH_URL}', headers=osm_headers, content=dumps(new_place))
    assert resp.status_code == 200

    resp = client.patch(f'{ASSOCIATE_EDENRED_URL}/{associate_id}', headers=associate_headers,
                        content=dumps({'osm_id': SAMPLE_OSM_PLACE["id"]}))
    assert resp.status_code == 200

    resp = client.get(f'{ASSOCIATE_EDENRED_URL}?query=osm_way', headers=associate_headers)
    assert resp.status_code == 200
    assert len(resp.json()) == 1
