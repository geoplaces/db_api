from json import dumps

import pytest

from test.conftest import OSM_PLACES_BATCH_URL, OSM_PLACES_URL, ASSOCIATE_DEJUP_URL, ASSOCIATE_EDENRED_URL
from test.test_dejup import add_dejup_place
from test.test_edenred import add_edenred_place

SAMPLE_OSM_PLACE = {
    'id': 'test',
    'osm_type': 'node',
    'name': 'test_name',
    'house_number': '3',
    'road': 'street',
    'town': 'city',
    'postcode': '11111',
    'lat': 100,
    'long': 100,
    'updated_at': "2023-01-12T18:21:41.920Z"
}


@pytest.fixture(scope='function')
def add_osm_place(client, osm_headers):
    places = [SAMPLE_OSM_PLACE.copy()]
    client.post(f'{OSM_PLACES_BATCH_URL}', headers=osm_headers, content=dumps(places))


def test_get_place(add_osm_place, client, token):
    headers = {'Authorization': 'Bearer ' +
                                token('read:osm')}
    resp = client.get(f'{OSM_PLACES_URL}/{SAMPLE_OSM_PLACE["id"]}', headers=headers)
    assert resp.status_code == 200

    resp = client.get(
        f'{OSM_PLACES_URL}/{SAMPLE_OSM_PLACE["id"] + "test"}', headers=headers)
    assert resp.status_code == 404


def test_post_places(client, osm_headers):
    places = [SAMPLE_OSM_PLACE.copy()]

    resp = client.post(f'{OSM_PLACES_BATCH_URL}', headers=osm_headers, content=dumps(places))
    assert resp.status_code == 201

    places[0]['name'] = SAMPLE_OSM_PLACE['name'] + 'wrong'
    resp = client.post(f'{OSM_PLACES_BATCH_URL}', headers=osm_headers, content=dumps(places))
    assert resp.status_code == 201

    resp = client.get(f'{OSM_PLACES_URL}/{SAMPLE_OSM_PLACE["id"]}', headers=osm_headers)
    # PUT request should NOT update the record
    assert resp.json().get('name') == SAMPLE_OSM_PLACE['name']


def test_update_place(add_osm_place, client, osm_headers):
    place = [SAMPLE_OSM_PLACE.copy()]
    place[0]['name'] = SAMPLE_OSM_PLACE['name'] + ' ok'
    resp = client.put(f'{OSM_PLACES_BATCH_URL}', headers=osm_headers, content=dumps(place))
    assert resp.status_code == 200
    resp = client.get(f'{OSM_PLACES_URL}/{SAMPLE_OSM_PLACE["id"]}', headers=osm_headers)
    assert resp.json().get('name') == SAMPLE_OSM_PLACE['name'] + ' ok'


def test_add_new_place_with_put(client, osm_headers):
    place = [SAMPLE_OSM_PLACE.copy()]
    resp = client.put(f'{OSM_PLACES_BATCH_URL}', headers=osm_headers, content=dumps(place))
    assert resp.status_code == 200


def test_delete_place(add_osm_place, add_dejup_place, add_edenred_place, client, osm_headers, associate_headers):
    resp = client.get(f'{ASSOCIATE_DEJUP_URL}?query=no_osm', headers=associate_headers)
    assert resp.status_code == 200
    associate_id = resp.json()[0]["associate_id"]

    resp = client.patch(f'{ASSOCIATE_DEJUP_URL}/{associate_id}', headers=associate_headers,
                        content=dumps({'osm_id': SAMPLE_OSM_PLACE["id"]}))
    assert resp.status_code == 200

    resp = client.get(f'{ASSOCIATE_EDENRED_URL}?query=no_osm', headers=associate_headers)
    assert resp.status_code == 200
    associate_id = resp.json()[0]["associate_id"]
    resp = client.patch(f'{ASSOCIATE_EDENRED_URL}/{associate_id}', headers=associate_headers,
                        content=dumps({'osm_id': SAMPLE_OSM_PLACE["id"]}))
    assert resp.status_code == 200

    payload = SAMPLE_OSM_PLACE["id"]
    resp = client.delete(f'{OSM_PLACES_URL}/{payload}', headers=osm_headers)
    assert resp.status_code == 204
