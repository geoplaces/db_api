from json import dumps

import pytest

from test.conftest import GOOGLE_PLACES_BATCH_URL, GOOGLE_PLACES_URL, ASSOCIATE_DEJUP_URL, ASSOCIATE_EDENRED_URL
from test.test_dejup import add_dejup_place
from test.test_edenred import add_edenred_place

SAMPLE_GOOGLE_PLACE = {
    'id': 'test',
    'name': 'test_name',
    'address': '3, Street City',
    'lat': 100,
    'long': 100,
    'updated_at': "2023-01-12T18:21:41.920Z"
}


@pytest.fixture(scope='function')
def add_google_place(client, token):
    headers = {'Authorization': 'Bearer ' +
                                token('write:google read:google')}
    places = [SAMPLE_GOOGLE_PLACE.copy()]
    client.post(f'{GOOGLE_PLACES_BATCH_URL}', headers=headers, content=dumps(places))


def test_get_place(add_google_place, client, token):
    headers = {'Authorization': 'Bearer ' +
                                token('read:google')}
    resp = client.get(
        f'{GOOGLE_PLACES_URL}/{SAMPLE_GOOGLE_PLACE["id"]}', headers=headers)
    assert resp.status_code == 200

    resp = client.get(
        f'{GOOGLE_PLACES_URL}/{SAMPLE_GOOGLE_PLACE["id"] + "test"}', headers=headers)
    assert resp.status_code == 404


def test_post_places(client, google_headers):
    places = [SAMPLE_GOOGLE_PLACE.copy()]

    resp = client.post(f'{GOOGLE_PLACES_BATCH_URL}', headers=google_headers, content=dumps(places))
    assert resp.status_code == 201

    places[0]['name'] = SAMPLE_GOOGLE_PLACE['name'] + 'wrong'
    resp = client.post(f'{GOOGLE_PLACES_BATCH_URL}', headers=google_headers, content=dumps(places))
    assert resp.status_code == 201

    resp = client.get(
        f'{GOOGLE_PLACES_URL}/{SAMPLE_GOOGLE_PLACE["id"]}', headers=google_headers)
    # PUT request should NOT update the record
    assert resp.json().get('name') == SAMPLE_GOOGLE_PLACE['name']


def test_update_place(add_google_place, client, google_headers):
    places = [SAMPLE_GOOGLE_PLACE.copy()]
    places[0]['name'] = SAMPLE_GOOGLE_PLACE['name'] + ' ok'
    resp = client.put(f'{GOOGLE_PLACES_BATCH_URL}', headers=google_headers, content=dumps(places))
    assert resp.status_code == 200
    resp = client.get(
        f'{GOOGLE_PLACES_URL}/{SAMPLE_GOOGLE_PLACE["id"]}', headers=google_headers)
    assert resp.json().get('name') == SAMPLE_GOOGLE_PLACE['name'] + ' ok'


def test_add_new_place_with_put(client, token, google_headers):
    places = [SAMPLE_GOOGLE_PLACE.copy()]
    resp = client.put(f'{GOOGLE_PLACES_BATCH_URL}', headers=google_headers, content=dumps(places))
    assert resp.status_code == 200


def test_delete_place(add_google_place, add_dejup_place, add_edenred_place, client, associate_headers, google_headers):
    resp = client.get(f'{ASSOCIATE_DEJUP_URL}?query=no_google', headers=associate_headers)
    assert resp.status_code == 200
    assert len(resp.json()) == 1
    associate_id = resp.json()[0]["associate_id"]

    resp = client.patch(f'{ASSOCIATE_DEJUP_URL}/{associate_id}', headers=associate_headers,
                        content=dumps({'gomaps_id': SAMPLE_GOOGLE_PLACE["id"]}))
    assert resp.status_code == 200
    resp = client.get(f'{ASSOCIATE_DEJUP_URL}?query=no_google', headers=associate_headers)
    assert resp.status_code == 200
    assert len(resp.json()) == 0

    resp = client.get(f'{ASSOCIATE_EDENRED_URL}?query=no_google', headers=associate_headers)
    assert resp.status_code == 200
    associate_id = resp.json()[0]["associate_id"]
    resp = client.patch(f'{ASSOCIATE_EDENRED_URL}/{associate_id}', headers=associate_headers,
                        content=dumps({'gomaps_id': SAMPLE_GOOGLE_PLACE["id"]}))
    assert resp.status_code == 200

    payload = SAMPLE_GOOGLE_PLACE["id"]
    resp = client.delete(f'{GOOGLE_PLACES_URL}/{payload}', headers=google_headers)
    assert resp.status_code == 204
