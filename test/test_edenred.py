from datetime import datetime, timedelta
from json import dumps

import pytest

from test.conftest import EDENRED_LOC_URL, EDENRED_PLACES_BATCH_URL, EDENRED_PLACES_URL, EDENRED_PLACES_COORDS_URL, \
    VERSION, LOC_PATH


@pytest.fixture(scope='function')
def add_edenred_location(client, edenred_loc_headers):
    client.post(EDENRED_LOC_URL, headers=edenred_loc_headers,
                content=dumps({"lat": 48.883376, "long": 2.215589, "radius": 5}))


class TestEdenredLocations:
    """Test indecies endpoints."""

    def test_location_post(self, client, edenred_loc_headers):
        resp = client.post(EDENRED_LOC_URL, headers=edenred_loc_headers,
                           content=dumps({"lat": 48.883376, "long": 2.215589, "radius": 5}))
        assert resp.status_code == 201

        resp = client.get(EDENRED_LOC_URL, headers=edenred_loc_headers)
        assert resp.status_code == 200
        assert len(resp.json()) == 1
        assert resp.json()[0]['radius'] == 5

        resp = client.post(EDENRED_LOC_URL, headers=edenred_loc_headers,
                           content=dumps({"l": 48.883376, "lo": 2.215589, "radius": 5}))
        assert resp.status_code == 422

        resp = client.post(f'{VERSION}{LOC_PATH}/wrong', headers=edenred_loc_headers,
                           content=dumps({"lat": 48.883376, "long": 2.215589, "radius": 5}))
        assert resp.status_code == 422

    def test_location_update(self, client, edenred_loc_headers, add_edenred_location):
        resp = client.get(f'{EDENRED_LOC_URL}?query=not_updated', headers=edenred_loc_headers)
        assert resp.status_code == 200
        assert len(resp.json()) == 1
        index = resp.json()[0]['index']

        resp = client.get(f'{VERSION}{LOC_PATH}/wrong?query=not_updated', headers=edenred_loc_headers)
        assert resp.status_code == 422

        resp = client.patch(f'{EDENRED_LOC_URL}/{index}', headers=edenred_loc_headers, content=dumps(
            {'updated_at': (datetime.now() - timedelta(days=3)).strftime('%Y-%m-%dT%H:%M:%SZ')}))
        assert resp.status_code == 200

        resp = client.get(f'{EDENRED_LOC_URL}?query=not_updated', headers=edenred_loc_headers)
        assert resp.status_code == 200
        assert len(resp.json()) == 0

    def test_location_access(self, client, token, add_edenred_location):
        headers = {'Authorization': 'Bearer ' + token('read:edenred_locations')}
        resp = client.get(EDENRED_LOC_URL, headers=headers)
        assert resp.status_code == 200
        assert len(resp.json()) == 1
        index = resp.json()[0]['index']

        resp = client.get(f'{VERSION}{LOC_PATH}/wrong', headers=headers)
        assert resp.status_code == 422

        resp = client.patch(f'{EDENRED_LOC_URL}/{index}', headers=headers, content=dumps(
            {'updated_at': '2022-01-19T16:00:00+05:00'}))
        assert resp.status_code == 401

        headers = {'Authorization': 'Bearer ' + token('me')}
        resp = client.get(EDENRED_LOC_URL, headers=headers)
        assert resp.status_code == 401

        resp = client.delete(f'{EDENRED_LOC_URL}/91190', headers=headers)
        assert resp.status_code == 401

    def test_location_delete(self, client, edenred_loc_headers, add_edenred_location):
        resp = client.get(EDENRED_LOC_URL, headers=edenred_loc_headers)
        index = resp.json()[0]['index']

        resp = client.delete(f'{EDENRED_LOC_URL}/{index}', headers=edenred_loc_headers)
        assert resp.status_code == 204
        resp = client.get(EDENRED_LOC_URL, headers=edenred_loc_headers)
        assert resp.status_code == 200
        assert len(resp.json()) == 0

        resp = client.delete(f'{EDENRED_LOC_URL}/{index}', headers=edenred_loc_headers)
        assert resp.status_code == 404


@pytest.fixture(scope='function')
def add_edenred_place(client, edenred_headers):
    place = [TestEdenredPlaces.SAMPLE_EDENRED_PLACE.copy()]
    client.post(EDENRED_PLACES_BATCH_URL, headers=edenred_headers, content=dumps(place))


class TestEdenredPlaces:
    SAMPLE_EDENRED_PLACE = {
        "name": "string",
        "address": "string",
        "zipcode": 0,
        "city": "string",
        "updated_at": "2023-01-12T18:21:41.920Z",
        "id": "00",
        "lat": 48.0,
        "long": 2.0
    }

    def test_add_places(self, client, edenred_headers):
        place = [self.SAMPLE_EDENRED_PLACE.copy()]

        place[0]['name'] = 'test'
        resp = client.post(EDENRED_PLACES_BATCH_URL, headers=edenred_headers,
                           content=dumps(place))
        assert resp.status_code == 201

        resp = client.get(
            f'{EDENRED_PLACES_URL}/{self.SAMPLE_EDENRED_PLACE["id"]}', headers=edenred_headers)
        assert resp.json().get('name') == 'test'

    def test_get_place(self, client, token, add_edenred_place):
        headers = {'Authorization': 'Bearer ' +
                                    token('read:edenred')}
        resp = client.get(
            f'{EDENRED_PLACES_URL}/{self.SAMPLE_EDENRED_PLACE["id"]}', headers=headers)
        assert resp.status_code == 200

        resp = client.get(
            f'{EDENRED_PLACES_URL}/{self.SAMPLE_EDENRED_PLACE["id"] + "1"}', headers=headers)
        assert resp.status_code == 404

    def test_delete_places(self, client, edenred_headers, add_edenred_place):
        resp = client.delete(f'{EDENRED_PLACES_URL}/{self.SAMPLE_EDENRED_PLACE["id"]}', headers=edenred_headers)
        assert resp.status_code == 204

    def test_delete_old(self, client, edenred_headers):
        place = [self.SAMPLE_EDENRED_PLACE.copy()]
        client.put(EDENRED_PLACES_URL, headers=edenred_headers,
                   content=dumps(place))

        resp = client.delete(f'{EDENRED_PLACES_BATCH_URL}/old', params={'days': 5},
                             headers=edenred_headers)
        assert resp.status_code == 204

        resp = client.get(f'{EDENRED_PLACES_URL}/{self.SAMPLE_EDENRED_PLACE["id"]}', headers=edenred_headers)
        assert resp.status_code == 404

    def test_update_places(self, client, edenred_headers, add_edenred_place):
        place = [self.SAMPLE_EDENRED_PLACE.copy()]
        place[0]['name'] = 'test'

        resp = client.put(EDENRED_PLACES_BATCH_URL,
                          headers=edenred_headers, content=dumps(place))
        assert resp.status_code == 200

        resp = client.get(
            f'{EDENRED_PLACES_URL}/{self.SAMPLE_EDENRED_PLACE["id"]}', headers=edenred_headers)
        assert resp.json().get('name') == 'test'

        place[0]['id'] = self.SAMPLE_EDENRED_PLACE["id"] + "1"

        resp = client.put(EDENRED_PLACES_BATCH_URL,
                          headers=edenred_headers, content=dumps(place))
        assert resp.status_code == 200

    def test_within_coords(self, client, token, add_edenred_place):
        params = {'n': self.SAMPLE_EDENRED_PLACE['lat'] + 1,
                  's': self.SAMPLE_EDENRED_PLACE['lat'] - 1,
                  'e': self.SAMPLE_EDENRED_PLACE['long'] + 1,
                  'w': self.SAMPLE_EDENRED_PLACE['long'] - 1,
                  }
        resp = client.get(f'{EDENRED_PLACES_COORDS_URL}', params=params)
        assert resp.status_code == 200
        assert resp.json()['total'] > 0

        params = {'n': self.SAMPLE_EDENRED_PLACE['lat'] + 2,
                  's': self.SAMPLE_EDENRED_PLACE['lat'] + 1,
                  'e': self.SAMPLE_EDENRED_PLACE['long'] + 1,
                  'w': self.SAMPLE_EDENRED_PLACE['long'] - 1,
                  }
        resp = client.get(f'{EDENRED_PLACES_COORDS_URL}', params=params)
        assert resp.status_code == 200
        assert resp.json()['total'] == 0
