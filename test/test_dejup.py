from datetime import datetime, timedelta
from json import dumps

import pytest

from app.v1.associate_utils import xy_to_coordinates
from test.conftest import DEJUP_LOC_URL, DEJUP_PLACES_BATCH_URL, DEJUP_PLACES_URL, DEJUP_PLACES_COORDS_URL


@pytest.fixture(scope='function')
def add_dejup_location(client, dejup_loc_headers):
    resp = client.post(f'{DEJUP_LOC_URL}', headers=dejup_loc_headers,
                       content=dumps({'zipcode': 91190}))
    assert resp.status_code == 201


class TestLocations:
    """Test locations endpoints."""

    def test_location_post(self, client, dejup_loc_headers):
        resp = client.post(f'{DEJUP_LOC_URL}', headers=dejup_loc_headers,
                           content=dumps({'zipcode': 91190}))
        assert resp.status_code == 201

        resp = client.get(f'{DEJUP_LOC_URL}', headers=dejup_loc_headers)
        assert resp.status_code == 200
        assert len(resp.json()) == 1

        resp = client.get(f'{DEJUP_LOC_URL}?query=not_updated', headers=dejup_loc_headers)
        assert resp.status_code == 200
        assert len(resp.json()) == 1

    def test_location_get(self, client, dejup_loc_headers, add_dejup_location):
        resp = client.get(f'{DEJUP_LOC_URL}?query=wrong', headers=dejup_loc_headers)
        assert resp.status_code == 422

        resp = client.get(f'{DEJUP_LOC_URL}?query=not_updated', headers=dejup_loc_headers)
        assert resp.status_code == 200
        assert len(resp.json()) == 1

    def test_location_date_update(self, client, dejup_loc_headers, add_dejup_location):
        resp = client.patch(f'{DEJUP_LOC_URL}/91190', headers=dejup_loc_headers, content=dumps(
            {'updated_at': (datetime.now() - timedelta(days=3)).strftime('%Y-%m-%dT%H:%M:%SZ')}))
        assert resp.status_code == 200

        resp = client.get(f'{DEJUP_LOC_URL}?query=not_updated', headers=dejup_loc_headers)
        assert resp.status_code == 200
        assert len(resp.json()) == 0

        resp = client.get(
            f'{DEJUP_LOC_URL}?query=outdated&outdated=0', headers=dejup_loc_headers)
        assert resp.status_code == 200
        assert len(resp.json()) == 1

        resp = client.get(
            f'{DEJUP_LOC_URL}?query=outdated&outdated=100', headers=dejup_loc_headers)
        assert resp.status_code == 200
        assert len(resp.json()) == 0

        resp = client.get(f'{DEJUP_LOC_URL}', headers=dejup_loc_headers)
        assert resp.status_code == 200
        assert len(resp.json()) == 1

    def test_location_update_wrong_schema(self, client, dejup_loc_headers, add_dejup_location):
        resp = client.patch(f'{DEJUP_LOC_URL}/91190', headers=dejup_loc_headers, content=dumps(
            {'wrong': (datetime.now() - timedelta(days=3)).strftime('%Y-%m-%dT%H:%M:%SZ')}))
        assert resp.status_code == 422

    def test_location_update_wrong_index(self, client, dejup_loc_headers, add_dejup_location):
        resp = client.patch(f'{DEJUP_LOC_URL}/91191', headers=dejup_loc_headers, content=dumps(
            {'updated_at': (datetime.now() - timedelta(days=3)).strftime('%Y-%m-%dT%H:%M:%SZ')}))
        assert resp.status_code == 404

    def test_location_ro_access(self, client, token, add_dejup_location):
        headers = {'Authorization': 'Bearer ' + token('read:dejup_locations')}
        resp = client.get(f'{DEJUP_LOC_URL}', headers=headers)
        assert resp.status_code == 200
        assert len(resp.json()) == 1

        resp = client.patch(f'{DEJUP_LOC_URL}/91190', headers=headers, content=dumps(
            {'updated_at': '2022-01-19T16:00:00+05:00'}))
        assert resp.status_code == 401

    def test_location_wrong_token(self, client, add_dejup_location, token):
        headers = {'Authorization': 'Bearer ' + token('me')}
        resp = client.get(f'{DEJUP_LOC_URL}', headers=headers)
        assert resp.status_code == 401

        resp = client.delete(f'{DEJUP_LOC_URL}/91190', headers=headers)
        assert resp.status_code == 401

    def test_location_delete(self, client, dejup_loc_headers, add_dejup_location):
        resp = client.delete(f'{DEJUP_LOC_URL}/91190', headers=dejup_loc_headers)
        assert resp.status_code == 204

        resp = client.get(f'{DEJUP_LOC_URL}', headers=dejup_loc_headers)
        assert resp.status_code == 200
        assert len(resp.json()) == 0

        resp = client.delete(f'{DEJUP_LOC_URL}/91190', headers=dejup_loc_headers)
        assert resp.status_code == 404


@pytest.fixture(scope='function')
def add_dejup_place(client, dejup_headers):
    places = [TestDejupPlaces.SAMPLE_DEJUP_PLACE.copy()]
    resp = client.post(f'{DEJUP_PLACES_BATCH_URL}', headers=dejup_headers, content=dumps(places))
    assert resp.status_code == 201


class TestDejupPlaces:
    SAMPLE_DEJUP_PLACE = {
        "link": "string",
        "name": "string",
        "address": "string",
        "zipcode": 0,
        "city": "string",
        "updated_at": "2023-01-12T18:21:41.920Z",
        "id": 0,
        "x": 5,
        "y": 5,
        "categoryName": "french"
    }

    def test_add_places(self, client, dejup_headers):
        places = [self.SAMPLE_DEJUP_PLACE.copy()]
        places[0]['name'] = 'test'
        resp = client.post(f'{DEJUP_PLACES_BATCH_URL}', headers=dejup_headers,
                           content=dumps(places))
        assert resp.status_code == 201

        resp = client.get(
            f'{DEJUP_PLACES_URL}/{self.SAMPLE_DEJUP_PLACE["id"]}', headers=dejup_headers)
        assert resp.json().get('name') == 'test'

    def test_add_empty_request(self, client, dejup_headers):
        resp = client.post(f'{DEJUP_PLACES_BATCH_URL}', headers=dejup_headers,
                           content=dumps([]))
        assert resp.status_code == 201

    def test_update_empty_request(self, client, dejup_headers):
        resp = client.put(f'{DEJUP_PLACES_BATCH_URL}', headers=dejup_headers,
                          content=dumps([]))
        assert resp.status_code == 200

    def test_get_place(self, client, token, add_dejup_place):
        headers = {'Authorization': 'Bearer ' +
                                    token('read:dejup')}
        resp = client.get(
            f'{DEJUP_PLACES_URL}/{self.SAMPLE_DEJUP_PLACE["id"]}', headers=headers)
        assert resp.status_code == 200

        resp = client.get(
            f'{DEJUP_PLACES_URL}/{self.SAMPLE_DEJUP_PLACE["id"] + 1}', headers=headers)
        assert resp.status_code == 404

    def test_delete_places(self, client, dejup_headers, add_dejup_place):
        resp = client.delete(f'{DEJUP_PLACES_URL}/{self.SAMPLE_DEJUP_PLACE["id"]}', headers=dejup_headers)
        assert resp.status_code == 204

    def test_delete_old(self, client, dejup_headers, add_dejup_place):
        resp = client.delete(f'{DEJUP_PLACES_BATCH_URL}/old', params={'days': 5},
                             headers=dejup_headers)
        assert resp.status_code == 204

        resp = client.get(f'{DEJUP_PLACES_URL}/{self.SAMPLE_DEJUP_PLACE["id"]}', headers=dejup_headers)
        assert resp.status_code == 404

    def test_update_places(self, client, dejup_headers, add_dejup_place):
        places = [self.SAMPLE_DEJUP_PLACE.copy()]
        places[0]['name'] = 'test'
        resp = client.put(f'{DEJUP_PLACES_BATCH_URL}',
                          headers=dejup_headers, content=dumps(places))
        assert resp.status_code == 200

        resp = client.get(
            f'{DEJUP_PLACES_URL}/{self.SAMPLE_DEJUP_PLACE["id"]}', headers=dejup_headers)
        assert resp.status_code == 200
        assert resp.json().get('name') == 'test'

    def test_place_create_with_put(self, client, dejup_headers):
        places = [self.SAMPLE_DEJUP_PLACE.copy()]
        places[0]['id'] = self.SAMPLE_DEJUP_PLACE["id"] + 1
        resp = client.put(f'{DEJUP_PLACES_BATCH_URL}',
                          headers=dejup_headers, content=dumps(places))
        assert resp.status_code == 200

    def test_create_multiple_places_with_put(self, client, dejup_headers):
        places = [self.SAMPLE_DEJUP_PLACE.copy(), self.SAMPLE_DEJUP_PLACE.copy()]
        places[1]['id'] = self.SAMPLE_DEJUP_PLACE["id"] + 1
        resp = client.put(f'{DEJUP_PLACES_BATCH_URL}',
                          headers=dejup_headers, content=dumps(places))
        assert resp.status_code == 200
        places[0]['name'] = 'test0'
        places[1]['name'] = 'test1'
        resp = client.put(f'{DEJUP_PLACES_BATCH_URL}',
                          headers=dejup_headers, content=dumps(places))
        assert resp.status_code == 200
        resp = client.get(
            f'{DEJUP_PLACES_URL}/{self.SAMPLE_DEJUP_PLACE["id"]}', headers=dejup_headers)
        assert resp.status_code == 200
        assert resp.json().get('name') == 'test0'
        resp = client.get(
            f'{DEJUP_PLACES_URL}/{self.SAMPLE_DEJUP_PLACE["id"] + 1}', headers=dejup_headers)
        assert resp.status_code == 200
        assert resp.json().get('name') == 'test1'

    def test_within_coords(self, client, token, add_dejup_place):
        lon, lat = xy_to_coordinates( self.SAMPLE_DEJUP_PLACE['x'],  self.SAMPLE_DEJUP_PLACE['y'])
        params = {'n': lat + 1,
                  's': lat - 1,
                  'w': lon - 1,
                  'e': lon + 1,
                  }
        resp = client.get(f'{DEJUP_PLACES_COORDS_URL}', params=params)
        assert resp.status_code == 200
        assert resp.json()['total'] > 0

        params = {'n': lat + 2,
                  's': lat + 1,
                  'w': lon - 1,
                  'e': lon + 1,
                  }
        resp = client.get(f'{DEJUP_PLACES_COORDS_URL}', params=params)
        assert resp.status_code == 200
        assert resp.json()['total'] == 0
