import string
from os import getenv as ge
from random import choice
from typing import Generator, Any

import pytest
from dotenv import load_dotenv
from fastapi import FastAPI
from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.dialects.postgresql import insert
from sqlalchemy.orm import sessionmaker, Session
from werkzeug.security import generate_password_hash

from app import app, app_init
from app.db import get_db, Base
from app.models import User

load_dotenv()

USER = 'admin'
PASW = 'test'

VERSION = '/v1'
TOKEN_PATH = f'{VERSION}/auth/token'
ME_PATH = f'{VERSION}/auth/users/me'

LOC_PATH = '/locations'
PLACES_PATH = '/places'

DEJUP_PATH = '/dejup'
DEJUP_LOC_URL = f'{VERSION}{LOC_PATH}{DEJUP_PATH}'
DEJUP_PLACES_URL = f'{VERSION}{PLACES_PATH}{DEJUP_PATH}'
DEJUP_PLACES_BATCH_URL = f'{VERSION}{PLACES_PATH}{DEJUP_PATH}/batch'
DEJUP_PLACES_COORDS_URL = f'{VERSION}{DEJUP_PATH}/places_coords'

EDENRED_PATH = '/edenred'
EDENRED_LOC_URL = f'{VERSION}{LOC_PATH}{EDENRED_PATH}'
EDENRED_PLACES_URL = f'{VERSION}{PLACES_PATH}{EDENRED_PATH}'
EDENRED_PLACES_BATCH_URL = f'{VERSION}{PLACES_PATH}{EDENRED_PATH}/batch'
EDENRED_PLACES_COORDS_URL = f'{VERSION}{EDENRED_PATH}/places_coords'

GOOGLE_PATH = '/google'
GOOGLE_PLACES_URL = f'{VERSION}{PLACES_PATH}{GOOGLE_PATH}'
GOOGLE_PLACES_BATCH_URL = f'{VERSION}{PLACES_PATH}{GOOGLE_PATH}/batch'

OSM_PATH = '/osm'
OSM_PLACES_URL = f'{VERSION}{PLACES_PATH}{OSM_PATH}'
OSM_PLACES_BATCH_URL = f'{VERSION}{PLACES_PATH}{OSM_PATH}/batch'

ASSOCIATE_PATH = '/associations'
ASSOCIATE_DEJUP_URL = f'{VERSION}{ASSOCIATE_PATH}{DEJUP_PATH}'
ASSOCIATE_EDENRED_URL = f'{VERSION}{ASSOCIATE_PATH}{EDENRED_PATH}'

SQLALCHEMY_DATABASE_URL = ge('DB_URL_TEST')

engine = create_engine(
    SQLALCHEMY_DATABASE_URL
)
# Use connect_args parameter only with sqlite
SessionTesting = sessionmaker(autocommit=False, autoflush=False, bind=engine)


@pytest.fixture(scope="function")
def get_app():
    app_init()
    Base.metadata.create_all(engine)
    yield app


@pytest.fixture(scope="function")
def db_session(get_app: FastAPI) -> Generator[SessionTesting, Any, None]:
    connection = engine.connect()
    transaction = connection.begin()
    session = SessionTesting(bind=connection)
    yield session  # use the session in tests.
    session.close()
    transaction.rollback()
    connection.close()


@pytest.fixture(scope="function")
def client(get_app: FastAPI, db_session: SessionTesting):
    def _get_test_db():
        yield db_session

    app.dependency_overrides[get_db] = _get_test_db

    db: Session = next(_get_test_db())
    letters = string.ascii_lowercase
    salt = ''.join(choice(letters) for _ in range(16))
    encoded_password = generate_password_hash(PASW + salt)
    stmt = insert(User).values(name=USER, pasw=encoded_password, salt=salt)
    db.execute(stmt.on_conflict_do_nothing())
    db.commit()

    with TestClient(get_app) as client:
        yield client


@pytest.fixture(scope='function')
def token(client):
    def _get_token(scope):
        payload = f'username={USER}&password={PASW}&scope={scope}'
        headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        resp = client.post(f'{TOKEN_PATH}', headers=headers, content=payload)
        return resp.json().get('access_token')

    return _get_token


AUTH_TYPE = 'Bearer '


@pytest.fixture(scope='function')
def dejup_headers(client, token):
    headers = {'Authorization': AUTH_TYPE +
                                token('write:dejup read:dejup')}
    return headers


@pytest.fixture(scope='function')
def dejup_loc_headers(client, token):
    headers = {'Authorization': AUTH_TYPE +
                                token('write:dejup_locations read:dejup_locations')}
    return headers


@pytest.fixture(scope='function')
def edenred_headers(client, token):
    headers = {'Authorization': AUTH_TYPE +
                                token('write:edenred read:edenred')}
    return headers


@pytest.fixture(scope='function')
def edenred_loc_headers(client, token):
    headers = {'Authorization': AUTH_TYPE +
                                token('write:edenred_locations read:edenred_locations')}
    return headers


@pytest.fixture(scope='function')
def google_headers(client, token):
    headers = {'Authorization': AUTH_TYPE +
                                token('write:google read:google')}
    return headers


@pytest.fixture(scope='function')
def osm_headers(client, token):
    headers = {'Authorization': AUTH_TYPE +
                                token('write:osm read:osm')}
    return headers


@pytest.fixture(scope='function')
def associate_headers(client, token):
    headers = {'Authorization': f'{AUTH_TYPE}' +
                                token('write:associate read:associate')}
    return headers
