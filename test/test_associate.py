from json import dumps

import pytest

from test.conftest import ASSOCIATE_DEJUP_URL, ASSOCIATE_EDENRED_URL, VERSION, ASSOCIATE_PATH
from test.test_dejup import add_dejup_place, TestDejupPlaces
from test.test_google import SAMPLE_GOOGLE_PLACE, GOOGLE_PLACES_BATCH_URL
from test.test_osm import SAMPLE_OSM_PLACE, add_osm_place, OSM_PLACES_BATCH_URL


@pytest.fixture(scope='function')
def dejup_associate_id(client, associate_headers, add_dejup_place):
    resp = client.get(f'{ASSOCIATE_DEJUP_URL}?query=no_osm', headers=associate_headers)
    assert resp.status_code == 200
    yield resp.json()[0]['associate_id']


@pytest.fixture(scope='function')
def add_linked_osm(client, associate_headers, dejup_associate_id, osm_headers, add_osm_place):
    resp = client.patch(f'{ASSOCIATE_DEJUP_URL}/{dejup_associate_id}', headers=associate_headers,
                        content=dumps({'osm_id': SAMPLE_OSM_PLACE["id"]}))
    assert resp.status_code == 200


def test_dejup_no_osm(client, associate_headers, dejup_associate_id, osm_headers):
    # add associated OSM place --> empty list should be returned
    osm_place = SAMPLE_OSM_PLACE.copy()
    resp = client.post(f'{OSM_PLACES_BATCH_URL}', headers=osm_headers, content=dumps([osm_place]))
    assert resp.status_code == 201

    resp = client.patch(f'{ASSOCIATE_DEJUP_URL}/{dejup_associate_id}', headers=associate_headers,
                        content=dumps({'osm_id': SAMPLE_OSM_PLACE["id"]}))
    assert resp.status_code == 200

    resp = client.patch(f'{ASSOCIATE_DEJUP_URL}/{dejup_associate_id + 1}', headers=associate_headers,
                        content=dumps({'osm_id': SAMPLE_OSM_PLACE["id"]}))
    assert resp.status_code == 404

    resp = client.get(f'{ASSOCIATE_DEJUP_URL}?query=no_osm', headers=associate_headers)
    assert resp.status_code == 200
    assert len(resp.json()) == 0


def test_wrong_patch_link(client, associate_headers, dejup_associate_id):
    resp = client.patch(f'{ASSOCIATE_DEJUP_URL}/{dejup_associate_id}', headers=associate_headers,
                        content=dumps({'osm_id': '1'}))
    assert resp.status_code == 400

    resp = client.patch(f'{ASSOCIATE_DEJUP_URL}/{dejup_associate_id}', headers=associate_headers,
                        content=dumps({'gomaps_id': '1'}))
    assert resp.status_code == 400


def test_dejup_wrong_query(client, associate_headers, add_dejup_place):
    # test with 1 added dejup place --> should be returned
    resp = client.get(f'{ASSOCIATE_DEJUP_URL}?query=wrong', headers=associate_headers)
    assert resp.status_code == 422


def test_dejup_wrong_association(client, associate_headers, add_dejup_place):
    # test with 1 added dejup place --> should be returned
    resp = client.get(f'{VERSION}{ASSOCIATE_PATH}/wrong?query=no_osm', headers=associate_headers)
    assert resp.status_code == 422


def test_dejup_no_google(client, associate_headers, dejup_associate_id, google_headers):
    # add associated Google place --> empty list should be returned
    google_place = SAMPLE_GOOGLE_PLACE.copy()
    resp = client.post(f'{GOOGLE_PLACES_BATCH_URL}', headers=google_headers, content=dumps([google_place]))
    assert resp.status_code == 201

    resp = client.patch(f'{ASSOCIATE_DEJUP_URL}/{dejup_associate_id}', headers=associate_headers,
                        content=dumps({'gomaps_id': SAMPLE_GOOGLE_PLACE["id"]}))
    assert resp.status_code == 200

    resp = client.get(f'{ASSOCIATE_DEJUP_URL}?query=no_google', headers=associate_headers)
    assert resp.status_code == 200
    assert len(resp.json()) == 0


def test_dejup_osm_dummy(client, token, dejup_associate_id):
    headers_read = {'Authorization': 'Bearer ' + token('read:associate')}
    headers_write = {'Authorization': 'Bearer ' + token('write:associate')}

    # add dummy
    resp = client.post(f'{ASSOCIATE_DEJUP_URL}/{dejup_associate_id}/dummy/osm', headers=headers_write)
    assert resp.status_code == 200

    resp = client.get(f'{ASSOCIATE_DEJUP_URL}?query=osm_dummy', headers=headers_read)
    assert resp.status_code == 200
    assert len(resp.json()) == 1

    resp = client.get(f'{ASSOCIATE_DEJUP_URL}?query=osm_dummy_no_google', headers=headers_read)
    assert resp.status_code == 200
    assert len(resp.json()) == 1

    resp = client.post(f'{ASSOCIATE_DEJUP_URL}/{dejup_associate_id}/dummy/google', headers=headers_write)
    assert resp.status_code == 200

    resp = client.post(f'{ASSOCIATE_DEJUP_URL}/{dejup_associate_id}/dummy/wrong', headers=headers_write)
    assert resp.status_code == 422
    resp = client.post(f'{ASSOCIATE_DEJUP_URL}/{dejup_associate_id + 1}/dummy/google', headers=headers_write)
    assert resp.status_code == 404

    resp = client.get(f'{ASSOCIATE_DEJUP_URL}?query=osm_dummy', headers=headers_read)
    assert resp.status_code == 200
    assert len(resp.json()) == 1

    resp = client.get(f'{ASSOCIATE_DEJUP_URL}?query=osm_dummy_no_google', headers=headers_read)
    assert resp.status_code == 200
    assert len(resp.json()) == 0


# FRONT END
def test_coords(client, add_linked_osm):
    params = {'n': SAMPLE_OSM_PLACE['lat'] + 1,
              's': SAMPLE_OSM_PLACE['lat'] - 1,
              'e': SAMPLE_OSM_PLACE['long'] + 1,
              'w': SAMPLE_OSM_PLACE['long'] - 1
              }
    resp = client.get(f'{ASSOCIATE_DEJUP_URL}/within_coords', params=params)

    assert resp.status_code == 200
    assert resp.json().get('total') == 1
    assert len(resp.json().get('data')) == 1
    assert resp.json().get('data')[0]['lat'] == SAMPLE_OSM_PLACE['lat']
    assert resp.json().get('data')[0]['cat'] == TestDejupPlaces.SAMPLE_DEJUP_PLACE['categoryName']

    params = {'n': SAMPLE_OSM_PLACE['lat'] + 2,
              's': SAMPLE_OSM_PLACE['lat'] + 1,
              'e': SAMPLE_OSM_PLACE['long'] + 1,
              'w': SAMPLE_OSM_PLACE['long'] - 1
              }
    resp = client.get(f'{ASSOCIATE_DEJUP_URL}/within_coords', params=params)

    assert resp.status_code == 200
    assert resp.json().get('total') == 0
    assert len(resp.json().get('data')) == 0


def test_dejup_osm_way(client, osm_headers, add_osm_place, add_dejup_place, associate_headers):
    resp = client.get(f'{ASSOCIATE_DEJUP_URL}?query=osm_way', headers=associate_headers)
    assert resp.status_code == 200
    assert len(resp.json()) == 0

    resp = client.get(f'{ASSOCIATE_DEJUP_URL}?query=no_osm', headers=associate_headers)
    associate_id = resp.json()[0]["associate_id"]

    new_place = [SAMPLE_OSM_PLACE.copy()]
    new_place[0]['osm_type'] = 'way'

    resp = client.put(f'{OSM_PLACES_BATCH_URL}', headers=osm_headers, content=dumps(new_place))
    assert resp.status_code == 200

    resp = client.patch(f'{ASSOCIATE_DEJUP_URL}/{associate_id}', headers=associate_headers,
                        content=dumps({'osm_id': SAMPLE_OSM_PLACE["id"]}))
    assert resp.status_code == 200

    resp = client.get(f'{ASSOCIATE_DEJUP_URL}?query=osm_way', headers=associate_headers)
    assert resp.status_code == 200
    assert len(resp.json()) == 1


def test_dummy_google(add_dejup_place, client, associate_headers):
    # add dejup entry w/o link to OSM
    resp = client.get(f'{ASSOCIATE_DEJUP_URL}?query=no_google', headers=associate_headers)
    associate_id = resp.json()[0]["associate_id"]

    # link a dummy Google record
    resp = client.post(f'{ASSOCIATE_DEJUP_URL}/{associate_id}/dummy/google', headers=associate_headers)
    assert resp.status_code == 200

    resp = client.post(f'{ASSOCIATE_EDENRED_URL}/{associate_id}/dummy/google', headers=associate_headers)
    assert resp.status_code == 404

    resp = client.post(f'{ASSOCIATE_DEJUP_URL}/{associate_id + 1}/dummy/google', headers=associate_headers)
    assert resp.status_code == 404
