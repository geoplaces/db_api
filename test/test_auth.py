from json import dumps

from test.conftest import USER, PASW, ME_PATH, VERSION, TOKEN_PATH, DEJUP_LOC_URL


def test_status(client):
    response = client.get(f'{VERSION}/auth/status')
    assert response.status_code == 200
    assert response.json() == {'status': 'ok'}


def test_token(client):
    payload = f'username={USER}&password={PASW}&scope=me'
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    resp = client.post(f'{TOKEN_PATH}', headers=headers, content=payload)
    assert resp.status_code == 200
    assert len(resp.json().get('access_token')) > 0


def test_wrong_cred(client):
    payload = f'username={USER}&password={PASW + "as"}&scope=me'
    headers = {'Content-Type': 'application/x-www-form-urlencoded'}
    resp = client.post(f'{TOKEN_PATH}', headers=headers, content=payload)
    assert resp.status_code == 400

    payload = f'username={USER + "as"}&password={PASW + "as"}&scope=me'
    resp = client.post(f'{TOKEN_PATH}', headers=headers, content=payload)
    assert resp.status_code == 400


def test_me(client, token):
    headers = {'Authorization': 'Bearer ' + token('me')}
    resp = client.get(f'{ME_PATH}', headers=headers)
    assert resp.status_code == 200
    assert resp.json().get('name') == USER


def test_invalid_token(client, token):
    headers = {'Authorization': 'Bearer ' + token('me') + 'haha'}
    resp = client.get(f'{ME_PATH}', headers=headers)
    assert resp.status_code == 401


def test_scopes(client, token):
    headers = {'Authorization': 'Bearer ' + token('read:dejup_locations')}
    resp = client.get(f'{ME_PATH}', headers=headers)
    assert resp.status_code == 401
    resp = client.get(f'{DEJUP_LOC_URL}', headers=headers)
    assert resp.status_code == 200
    resp = client.post(f'{DEJUP_LOC_URL}', headers=headers, content=dumps({'zipcode': 91190}))
    assert resp.status_code == 401
    headers = {'Authorization': 'Bearer ' + token('write:dejup_locations')}
    resp = client.post(f'{DEJUP_LOC_URL}', headers=headers, content=dumps({'zipcode': 91190}))
    assert resp.status_code == 201
