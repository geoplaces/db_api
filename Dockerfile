FROM python:3.10.6-slim-buster

RUN apt-get update && apt-get install -y --no-install-recommends netcat  && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /usr/src/app
RUN mkdir -p /var/log/GeoPlaces

COPY . /usr/src/app/

RUN python3 -m pip install --upgrade pip
RUN python3 -m pip install -r requirements.txt

ENTRYPOINT ["/usr/src/app/entrypoint.sh"]
