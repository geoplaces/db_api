# Database API to store places info

A simple FastAPI framework to aggregate geo information from different sources
and provide combined data to various apps.

Use case scenario: scrap some service for the addresses and complement the data with geolocation: longitude and latitude.

App keeps the tables from different sources independent, the authorisation is done with token for a fixed scopes.

## Cold start

### Python venv

Python3 and PostgresSQL are required.

```bash
python3 -m venv vdb
. vdb/bin/activate
pip install -r requirements.txt
```

Preconfigure app providing DB credentials

```bash
cp .env_example .env
# edit .env
```

DB install

```bash
cd app
alembic upgrade head
```

Run the API client from the root folder

```bash
uvicorn app:app_init --host 0.0.0.0 --port 80
```

### Docker

Run a DB api at `localhost:80`

```bash
cp .env_example .env # UPDATE CREDENTIALS!!!
docker-compose build && docker-compose up
```

## Security

The auth is done with Bearer token. Create an API user with

```bash
python3 app/create_api_user.py -u test_name -p test_pass
```

The token will be served in the response to `/v1/auth/token`

```bash
curl -X POST 'http://0.0.0.0/v1/auth/token' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode 'username=test_name' \
--data-urlencode 'password=test_pass' \
--data-urlencode 'scope=me read:dejup_locations write:dejup_locations read:dejup write:dejup'
```

## API logic

POST request stores places in the table. If the unique key is
violated the new place is skipped, the old record is in place.

PUT request updates the place record. If no record with the
given key exists - it will be created.
