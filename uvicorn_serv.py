import uvicorn

if __name__ == "__main__":
    uvicorn.run("app:app_init", port=9000, reload=True)
